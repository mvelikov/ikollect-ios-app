//
//  LibraryAPI.h
//  iKollect
//
//  Created by mihata on 5/19/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Artwork.h"
#import "Gallery.h"
#import "Bucket.h"
#import "Tag.h"
#import "HTTPClient.h"


@protocol LibraryAPIDelegate;

@interface LibraryAPI : NSObject <HTTPClientDelegate>

@property (nonatomic, weak) id <LibraryAPIDelegate> delegate;
+ (LibraryAPI*)sharedInstance;

- (NSArray*) getArtworks;
- (Artwork*) getArtwork;
- (Artwork*) getDummyArtwork;
- (void) loadArtworks;
- (void) loadArtworksPage:(int) page;
- (void) voteForArtwork: (Artwork*) artwork withVote : (NSInteger) vote;
- (void) clearArtworks;
- (void) getGeoLocationForAddress: (NSString*)address;

- (NSArray*) getTags;
- (void) clearTags;
- (void) addTag: (Tag*) tag;

- (NSArray*) getBucketedArtworks;
- (void) clearBucketedArtworks;
- (void) addBucketedArtworks: (Artwork*) artwork;
- (int) totalBucketedArtworks;
- (int) currentPageBucketedArtworks;
- (int) perPageBucketedArtworks;
- (int) lastPageBucketedArtworks;


- (NSArray*) getBuckets;
- (void) clearBuckets;
- (void) addBucket: (Bucket*) bucket;
- (int) totalBuckets;
- (int) currentPageBuckets;
- (int) perPageBuckets;
- (int) lastPageBuckets;


- (void) loadGalleries;
- (void) loadTagsPage:(int) page;
- (void) addGallery: (Gallery*) gallery;
- (NSArray*) getGalleries;
- (void) loginUserWithEmail: (NSString*) email andPassword: (NSString*) password;
- (void) createCustomerAccountForFacebookUser:(id)user;

- (void) loadAppSettings;

- (void) loadBucketsListForUser: (NSString *) user;
- (void) loadBucketsListForUser: (NSString *) user atPage: (int) page;
- (void) createBucketWithName:(NSString*) name forUser: (NSString *) user;
- (void) addArtwork:(NSString *) artwork inBuckets: (NSArray *)buckets;
- (void) loadArtworksFromBucket: (NSString *)bucket;
- (void) loadArtworksFromBucket: (NSString *)bucket atPage: (NSUInteger) page;

- (void) loadArtworkWithId: (NSString *)uid;

- (void) loadLikedArtworksForUser: (NSString *) user atPage: (NSUInteger) page;

- (void) loadGalleryDetailsWithPlaceId:(NSString*) placeId;
@end

@protocol LibraryAPIDelegate <NSObject>

@optional

- (void) libraryDidLoadData;
- (void) libraryDidLoadDataAtCurrentPage: (int) currentPage outOf: (int) pages withTotalNumberOf: (int) totalNumber;
- (void) libraryDidLoadTags;
- (void) loadAddressWithLatitude: (float) lat AndLongitude: (float) lng;
- (void) libraryDidLoadGalleries;
- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error;
- (void) doUserLogin;
- (void) userLoginFailedWithMessage:(NSString*) message;

- (void) didLoadAppSettings:(NSDictionary *) settings;
- (void) libraryDidLoadBuckets;

- (void) libraryDidLoadBucketedArtworks;
- (void) libraryDidLoadArtwork:(Artwork *) artwork;
- (void) libraryDidLoadGalleryDetails: (id) galleryDetails;

@end