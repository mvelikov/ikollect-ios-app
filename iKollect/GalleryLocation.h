//
//  GalleryLocation.h
//  iKollect
//
//  Created by mihata on 7/23/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface GalleryLocation : NSObject <MKAnnotation>

@property (nonatomic, strong) NSString *placeId;

- (id)initWithTitle:(NSString*)title andSubtitle:(NSString*)subtitle andCoordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;

@end
