//
//  InstructionsViewController.h
//  iKollect
//
//  Created by mihata on 9/24/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface InstructionsViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UIView *mainView;
- (IBAction)doneButtonTapped:(id)sender;

@end
