//
//  ArtworkOverlayView.h
//  iKollect
//
//  Created by mihata on 5/14/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ArtworkOverlayViewMode) {
    ArtworkViewModeLeft,
    ArtworkViewModeRight
};
@interface ArtworkOverlayView : UIView
@property (nonatomic) ArtworkOverlayViewMode mode;
@end
