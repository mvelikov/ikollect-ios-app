//
//  ViewController.m
//  iKollect
//
//  Created by mihata on 5/10/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "MasterArtworkViewController.h"
#import "SWRevealViewController.h"
#import "Artwork.h"
#import "LibraryAPI.h"
#import "DetailsViewController.h"
#import "LoginViewController.h"

#import "TransitionDelegate.h"
#import <QuartzCore/QuartzCore.h>


@interface MasterArtworkViewController () {
    NSArray *artworkCollection;
    Artwork *artwork;
    BOOL dataDidLoad;
    CGFloat artworkViewX, artworkViewHeight;
    
    CLLocation *currentLocation;
    int currentPage, pages, totalArtworks;
    NSTimer *timer;
}

@end

@implementation MasterArtworkViewController

@synthesize artworkView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_voteButtonsWrapperView.layer setBorderColor:(__bridge CGColorRef)([UIColor colorWithRed:178/255 green:178/255 blue:178/255 alpha:178/255])];
    [_voteButtonsWrapperView.layer setBorderWidth:1.0 / [UIScreen mainScreen].scale];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    pages = 0;
    currentPage = 1;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isInstructionsScreenShown = [defaults boolForKey:@"isInstructionsScreenShown"];

    
    if (isInstructionsScreenShown == NO) {
        _transitionController = [[TransitionDelegate alloc] init];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        
        UIViewController *instructionsController = [storyboard instantiateViewControllerWithIdentifier:@"instructionsScreen"];
        
        [instructionsController setTransitioningDelegate:_transitionController];
        instructionsController.modalPresentationStyle= UIModalPresentationCustom;
        
        [self presentViewController:instructionsController animated:YES completion:nil];
        
        [defaults setBool:YES forKey:@"isInstructionsScreenShown"];
        [defaults synchronize];
    }
    
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    LibraryAPI *library = [LibraryAPI sharedInstance];
    library.delegate = self;
    
    [self hideDetailsAndVoteButtons];
    [library clearArtworks];
    [library loadArtworks];

    [_draggableViewWrapper setHidden:YES];
    [_loadingArtworkIndicator setHidden:NO];
    
    self.title = @"IKOLLECT";
    UIColor *globalTint = [[[UIApplication sharedApplication] delegate] window].tintColor;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:globalTint};
    
    artworkCollection = [[NSArray alloc] init];

    artworkViewHeight = self.view.frame.size.width - 20;
    artworkViewX = 20;
    if (IS_OS_8_OR_LATER) {
        artworkViewX = 10;
    }
    
    artworkView = [[ArtworkView alloc] initWithFrame:CGRectMake(artworkViewX, 30, self.view.frame.size.width - 20, artworkViewHeight)];

    artworkView.draggableView.delegate = self;

    [_draggableViewWrapper addSubview: artworkView];
    [_noMoreArtworksLabel setPreferredMaxLayoutWidth:self.view.frame.size.width - 40];
    
    [self registerTapRecognizerForArtworkView];
    
    [self checkIfGeoLocationIsEnabledAndStartService];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Master Artwork Screen";
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [[Helper sharedInstance] stopUpdatingLocation];
}

- (void) registerTapRecognizerForArtworkView
{
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(artworkViewTapped:)];
    [artworkView addGestureRecognizer:singleFingerTap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) hideDetailsAndVoteButtonsAndShowNoMoreArtworksMessage
{
    [self hideDetailsAndVoteButtons];
    [_noMoreArtworksLabel setHidden:NO];
    [_loadingArtworkIndicator setHidden:YES];
}
- (void) hideDetailsAndVoteButtons
{
    [_detailsButton setEnabled:NO];
    [_dislikeVoteButton setEnabled:NO];
    [_likeVoteButton setEnabled:NO];
    [_addToBucketButton setEnabled:NO];

    [_draggableViewWrapper setHidden:YES];
    [_loadingArtworkIndicator setHidden:NO];
    

}

- (void) showDetailsAndVoteButtons
{
    [_detailsButton setEnabled:YES];
    [_dislikeVoteButton setEnabled:YES];
    [_likeVoteButton setEnabled:YES];
    
    [_addToBucketButton setEnabled:YES];
    [_draggableViewWrapper setHidden:NO];
    [_loadingArtworkIndicator setHidden:YES];
    
    [_noMoreArtworksLabel setHidden:YES];
}
- (void) setArtworkViewSettingsByArtwork: (Artwork*)artworkObj
{
    [artworkView setTitle:[artworkObj title]];
    [artworkView setDimensions:[artworkObj getDimensionsString]];
    [artworkView setAuthor:[artworkObj author]];

    [artworkView setDraggableImage:[artworkObj thumbnail]];

    [artworkView loadDraggableCustomView];

    CGRect frame = CGRectMake(artworkViewX, 30, self.view.frame.size.width - 20, artworkViewHeight);
    [artworkView setFrame:frame];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showArtworkDetails"]
        || [[segue identifier] isEqualToString:@"showArtworkDetailsFromArtwork"]) {

        [[segue destinationViewController] setDetailsObject:artwork];
    } else if ([[segue identifier] isEqualToString:@"masterShowBucketsList"]) {
        BucketPickerTableViewController *controller = [segue destinationViewController];
        [controller setDelegate:self];
        [controller setBucketedArtworks:[artwork buckets]];
    }
    
}

//- (void) removeSwipedArtwork
- (void) artworkDraggableDidEndSwipingWithStrength:(CGFloat) strength;
{
    LibraryAPI *library = [LibraryAPI sharedInstance];
    
    strength *= 3;
    if ([artwork uid] != nil) {
        [library voteForArtwork:artwork withVote: lrintf(strength)];
    }
    
    long artworksCount = [[library getArtworks] count];
    
    if (artworksCount == 0 && currentPage < pages) {
        currentPage++;
        [library loadArtworksPage:currentPage];
        
        [self hideDetailsAndVoteButtons];
    }
    
    
    artwork = nil;
    artwork = [library getArtwork];
    // invalid artwork is loaded
    if ([artwork uid] == nil) {
        // If totalArtworks is lower or equal to 10 it means there are no more artworks to load
        if (totalArtworks <= 10) {
            [self hideDetailsAndVoteButtonsAndShowNoMoreArtworksMessage];
        } else {
            [self hideDetailsAndVoteButtons];
        }
    } else {
        [self setArtworkViewSettingsByArtwork:artwork];
    }
}

- (IBAction)artworkViewTapped:(id)sender {
    [self performSegueWithIdentifier:@"showArtworkDetails" sender:self];
}

- (IBAction)likeButtonTapped:(id)sender {

    [UIView animateWithDuration:0.4 animations:^{

        int x = (artworkView.draggableView.frame.size.width
                 - (artworkView.draggableView.frame.size.width * 0.5)) ;
        CGRect frame = CGRectMake(x,
                                  artworkView.draggableView.frame.origin.y - 30,
                                  artworkView.draggableView.frame.size.width,
                                  artworkView.draggableView.frame.size.height);
        [artworkView setFrame:frame];
    } completion:^(BOOL finished) {

        CGRect frame = CGRectMake(10,
                                  artworkView.draggableView.frame.origin.y + 30,
                                  artworkView.draggableView.frame.size.width,
                                  artworkView.draggableView.frame.size.height);
        [artworkView setFrame:frame];
        [self artworkDraggableDidEndSwipingWithStrength:1.0];
    }];
}

- (IBAction)dislikeButtonTapped:(id)sender {

    [UIView animateWithDuration:0.4 animations:^{

        int x = (artworkView.draggableView.frame.size.width
                 - (artworkView.draggableView.frame.size.width * 0.5)) * -1;
        CGRect frame = CGRectMake(x,
                                  artworkView.draggableView.frame.origin.y - 30,
                                  artworkView.draggableView.frame.size.width,
                                  artworkView.draggableView.frame.size.height);
        [artworkView setFrame:frame];
    } completion:^(BOOL finished) {

        CGRect frame = CGRectMake(10,
                                  artworkView.draggableView.frame.origin.y + 30,
                                  artworkView.draggableView.frame.size.width,
                                  artworkView.draggableView.frame.size.height);
        [artworkView setFrame:frame];
        [self artworkDraggableDidEndSwipingWithStrength:-1.0];
    }];

}

- (IBAction)closeInstructionsButtonTapped:(id)sender {
    [_instructionsImage setHidden:YES];
}

#pragma mark - LibraryAPIDelegate

- (void) libraryDidLoadDataAtCurrentPage: (int) current outOf: (int) pagesCount withTotalNumberOf:(int)totalNumber
{
    currentPage = current;
    pages = pagesCount;
    totalArtworks = totalNumber;

    artwork = [[LibraryAPI sharedInstance] getArtwork];
    if ([artwork uid] == nil) {
        [self hideDetailsAndVoteButtonsAndShowNoMoreArtworksMessage];
    } else {
        [self setArtworkViewSettingsByArtwork:artwork];

        [self showDetailsAndVoteButtons];
    }
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}

#pragma mark BucketPickerDelegate

- (void) userDidSelectBuckets: (NSArray*) selectedBuckets {

    if ([selectedBuckets count]) {
        [artwork setBuckets:[selectedBuckets mutableCopy]];
        [[LibraryAPI sharedInstance] addArtwork:[artwork uid] inBuckets:selectedBuckets];
    }
}

#pragma mark CLLocationManagerDelegate

- (void) checkIfGeoLocationIsEnabledAndStartService {

    UserSettings *userSettings = [[User sharedInstance] settings];
    
    if ([[userSettings city] isEqualToString:@"geoLocation"]) {
//        [[Helper sharedInstance] startStandardLocationServiceWithDelegate:self];
    }
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {

    [[Helper sharedInstance] locationManager:manager didUpdateLocations:locations];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [[Helper sharedInstance] locationManager:manager didFailWithError:error];
}


@end
