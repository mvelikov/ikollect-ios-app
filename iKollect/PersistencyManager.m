//
//  PersistencyManager.m
//  iKollect
//
//  Created by mihata on 5/19/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "PersistencyManager.h"

@interface PersistencyManager () {
    IKTCollection *artworks;
    IKTCollection *galleries;
    IKTCollection *tags;
    IKTCollection *buckets;
    IKTCollection *bucketedArtworks;
}

@end

@implementation PersistencyManager

- (id) init
{
    self = [super init];

    if (self) {
        artworks = [[IKTCollection alloc] init];
        galleries = [[IKTCollection alloc] init];
        buckets = [[IKTCollection alloc] init];
        tags = [[IKTCollection alloc] init];
        bucketedArtworks = [[IKTCollection alloc] init];
    }
    return self;
}

- (NSArray*) getArtworks
{
    return [artworks data];
}

- (Artwork*) getDummyArtwork
{
    Artwork *artwork = [[Artwork alloc] init];
    [artwork setTitle:@"Oops 404"];
    NSMutableArray *photos = [[NSMutableArray alloc] init];
    [photos addObject:@"http://res.cloudinary.com/ikollect/image/upload/v1403375922/pirate404_2x_gzlvt0.jpg"];
    [artwork setPhotos:photos];
    [artwork setDescription:@"There was an error, please excuse us!"];
    [artwork setAuthor:@"iKollect Team"];
    [artwork setGallery:[[Gallery alloc] init]];
    [artwork setTechnique:@"Midnight computing"];
    [artwork setUid:nil];
    NSMutableDictionary *dimension = [[NSMutableDictionary alloc] init];
    [dimension setObject:@"x" forKey:@"height"];
    [dimension setObject:@"x" forKey:@"width"];
    [dimension setObject:@"x" forKey:@"measure"];
    
    [artwork setDimensions: dimension];

    return artwork;
}
- (Artwork*) popArtwork
{
    Artwork *artworkObj = [[Artwork alloc] init];
    
    if ([artworks count] > 0) {
        artworkObj = [artworks objectAtIndex:0];
    
        [artworks removeObjectAtIndex:0];
    }
    return artworkObj;
}

- (void) setArtworks:(NSArray*)artworksList
{
    [self clearArtworks];
    [artworks setData: [artworksList mutableCopy]];
}

- (void) addArtwork: (Artwork*)artwork
{
    [artworks addObject:artwork];
}

- (void) clearArtworks
{
    [artworks removeAllObjects];
}

- (NSArray*) getGalleries
{
    return [galleries data];
}

- (void) addGallery: (Gallery*)gallery
{
    [galleries addObject:gallery];
}
- (void) clearGalleries
{
    [galleries removeAllObjects];
}

- (NSArray*) getTags
{
    return [tags data];
}

- (void) clearTags
{
    [tags removeAllObjects];
}

- (void) addTag: (Tag*) tag
{
    [tags addObject:tag];
}

- (void) addBucket:(Bucket *)bucket
{
    [buckets addObject:bucket];
}

- (void) setBuckets: (NSDictionary *) dataDictionary {
    NSArray *newBuckets = [dataDictionary valueForKey:@"data"];
    NSMutableArray *mergedArray = [[[buckets data] arrayByAddingObjectsFromArray:newBuckets] mutableCopy];
    [buckets setData:mergedArray];
    [buckets setTotal:[[dataDictionary valueForKey:@"total"] intValue]];
    [buckets setPerPage:[[dataDictionary valueForKey:@"per_page"] intValue]];
    [buckets setLastPage:[[dataDictionary valueForKey:@"last_page"] intValue]];
    [buckets setCurrentPage:[[dataDictionary valueForKey:@"current_page"] intValue]];
    [buckets setFrom:[[dataDictionary valueForKey:@"from"] intValue]];
    [buckets setTo:[[dataDictionary valueForKey:@"to"] intValue]];
}

- (NSArray *) getBuckets
{
    return [buckets data];
}

- (void) clearBuckets
{
    [buckets removeAllObjects];
    [buckets setTotal:0];
    [buckets setPerPage:0];
    [buckets setLastPage:0];
    [buckets setCurrentPage:0];
    [buckets setFrom:0];
    [buckets setTo:0];
}
- (int) totalBuckets {
    return [buckets total];
}
- (int) currentPageBuckets {
    return [buckets currentPage];
}
- (int) perPageBuckets {
    return [buckets perPage];
}
- (int) lastPageBuckets {
    return [buckets lastPage];
}

- (NSArray*) getBucketedArtworks {
    return [bucketedArtworks data];
}
- (void) clearBucketedArtworks {
    [bucketedArtworks removeAllObjects];
    [bucketedArtworks setTotal:0];
    [bucketedArtworks setPerPage:0];
    [bucketedArtworks setLastPage:0];
    [bucketedArtworks setCurrentPage:0];
    [bucketedArtworks setFrom:0];
    [bucketedArtworks setTo:0];
}
- (void) addBucketedArtworks: (Artwork*) artwork {
    [bucketedArtworks addObject:artwork];
}

- (void) setBucketedArtworks: (NSDictionary *) dataDictionary {
    NSArray *newBuckets = [dataDictionary valueForKey:@"data"];
    NSMutableArray *mergedArray = [[[bucketedArtworks data] arrayByAddingObjectsFromArray:newBuckets] mutableCopy];
    [bucketedArtworks setData:mergedArray];
    [bucketedArtworks setTotal:[[dataDictionary valueForKey:@"total"] intValue]];
    [bucketedArtworks setPerPage:[[dataDictionary valueForKey:@"per_page"] intValue]];
    [bucketedArtworks setLastPage:[[dataDictionary valueForKey:@"last_page"] intValue]];
    [bucketedArtworks setCurrentPage:[[dataDictionary valueForKey:@"current_page"] intValue]];
    [bucketedArtworks setFrom:[[dataDictionary valueForKey:@"from"] intValue]];
    [bucketedArtworks setTo:[[dataDictionary valueForKey:@"to"] intValue]];
}

- (int) totalBucketedArtworks {
    return [bucketedArtworks total];
}
- (int) currentPageBucketedArtworks {
    return [bucketedArtworks currentPage];
}
- (int) perPageBucketedArtworks {
    return [bucketedArtworks perPage];
}
- (int) lastPageBucketedArtworks {
    return [bucketedArtworks lastPage];
}

@end
