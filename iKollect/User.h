//
//  User.h
//  iKollect
//
//  Created by mihata on 5/25/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserSettings.h"

@interface User : NSObject
@property (nonatomic, strong) NSString *name, *email, *sex, *uid, *username, *loginType;
@property (nonatomic, strong) NSURL *avatar;
@property (nonatomic, strong) UserSettings *settings;
@property (nonatomic) BOOL isLogged;
//@property (nonatomic) int distance;
//@property (nonatomic, strong) CLLocation *currentLocation;

+ (User*)sharedInstance;

- (void) setUserUid: (NSString*) uid Name: (NSString*) name Username: (NSString*) username Email: (NSString*) email Avatar: (NSURL*) avatar AndSex: (NSString*) sex;
- (void) clearUser;

- (BOOL) loadUser;
- (void) saveUser;

- (void) saveUserSettings;
- (void) loadUserSettings;
@end
