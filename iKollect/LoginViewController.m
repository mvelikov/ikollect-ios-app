//
//  LoginViewController.m
//  iKollect
//
//  Created by mihata on 8/30/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "LoginViewController.h"
#import "AboutDetailsViewController.h"

@interface LoginViewController () {
    BOOL isFacebookUserLogged;
}

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _emailField.delegate = self;
    _passwordField.delegate = self;
    
    [LibraryAPI sharedInstance].delegate = self;

    _loginView.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    _loginView.delegate = self;
    
    isFacebookUserLogged = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self setRoundedCornersForViewElements];
    [self.view addGestureRecognizer:tap];
    
    [_errorMessageLabel setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.screenName = @"Login Screen";
}

-(void)dismissKeyboard {
    [_passwordField resignFirstResponder];
    [_emailField resignFirstResponder];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _passwordField) {
        [textField resignFirstResponder];
        [self loginUser];
    } else if(textField == _emailField) {
        [_passwordField becomeFirstResponder];
    }
    return YES;
}

- (void) disableFormFieldsAndButtonsAndPresentIndicator
{
    [_loginActivityIndicator setHidden:NO];
    [_emailField setEnabled:NO];
    [_passwordField setEnabled:NO];
    [_loginButton setEnabled:NO];
    [_loginView setHidden:YES];
    [_signupButton setEnabled:NO];
}

- (void) enableFormFieldsAndButtonsAndHideIndicator
{
    [_loginActivityIndicator setHidden:YES];
    [_emailField setEnabled:YES];
    [_passwordField setEnabled:YES];
    [_loginButton setEnabled:YES];
    [_loginView setHidden:NO];
    [_signupButton setEnabled:YES];
}

- (void) setRoundedCornersForViewElements
{
    float radius = 4.0f;
    [_loginButton.layer setCornerRadius:radius];
    [_emailLabel.layer setCornerRadius:radius];
    [_passwordLabel.layer setCornerRadius:radius];
    [_errorMessageLabel.layer setCornerRadius:radius];
    [_signupButton.layer setCornerRadius:radius];
    [_loginView.layer setCornerRadius:radius];
    [_titleLabel.layer setCornerRadius:radius];
}
- (void) loginUser
{

    _errorMessageLabel.text = @"";
    if ([_emailField.text isEqualToString:@""] || [_passwordField.text isEqualToString:@""]) {
        _errorMessageLabel.text = @"Please enter both email and password!";
        [_errorMessageLabel setHidden:NO];
    }
    else if ([self validateEmailWithString:_emailField.text]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            User *user = [User sharedInstance];
            [user setLoginType:LOGIN_TYPE_EMAIL];
            [user saveUser];
        });

        [self disableFormFieldsAndButtonsAndPresentIndicator];
        [[LibraryAPI sharedInstance] loginUserWithEmail: _emailField.text andPassword: _passwordField.text];
    } else {
        _errorMessageLabel.text = @"Invalid email address given!";
        [_errorMessageLabel setHidden:NO];
    }
    
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)loginButtonTapped:(id)sender
{
    [self loginUser];
}

//- (IBAction)signupButtonTapped:(id)sender {
//    NSString *signupURL = @"http://ikollect.cloudcontrolled.com/users/create";
//    
//    NSString *url = [signupURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
//    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
//}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showSignupPage"]) {
        AboutDetailsViewController *controller = [segue destinationViewController];
        controller.title = @"Terms of Use";
        controller.aboutUrl = [NSString stringWithFormat:@"%@/users/create", BASE_URL];
        
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:controller action:@selector(doneButtonTapped:)];
        controller.navigationItem.rightBarButtonItem = rightButton;


    }
}
#pragma mark LibraryAPIDelegate methods

- (void) doUserLogin
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_emailField.text forKey:@"email"];
    [defaults setObject:_passwordField.text forKey:@"password"];
    
    [defaults synchronize];
    
//    [self performSegueWithIdentifier:@"showLoggedUserArtworks" sender:self];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self enableFormFieldsAndButtonsAndHideIndicator];
    
}

- (void) userLoginFailedWithMessage:(NSString*) message
{
    _errorMessageLabel.text = message;
    [_errorMessageLabel setHidden:NO];
    [self enableFormFieldsAndButtonsAndHideIndicator];
    [_emailField becomeFirstResponder];
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
    [self enableFormFieldsAndButtonsAndHideIndicator];
}

#pragma mark FacebookSDK Delegate

// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {

    // NSLog(@"%@", user.name);
    if (isFacebookUserLogged == YES) {
        [[LibraryAPI sharedInstance] createCustomerAccountForFacebookUser:user];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            User *user = [User sharedInstance];
            [user setLoginType:LOGIN_TYPE_FACEBOOK];
            [user saveUser];
        });
    }
}

// Implement the loginViewShowingLoggedInUser: delegate method to modify your app's UI for a logged-in user experience
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {

    isFacebookUserLogged = YES;
    // NSLog(@"You're logged in");
}

// Implement the loginViewShowingLoggedOutUser: delegate method to modify your app's UI for a logged-out user experience
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {

    // NSLog(@"You're not logged in");
}

// You need to override loginView:handleError in order to handle possible errors that can occur during login
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures since that happen outside of the app.
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        // NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        // NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

@end
