//
//  IKTGalleryDetailsViewController.m
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/15/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTGalleryDetailsViewController.h"
#import "IKOLLECT-Swift.h"
#import "ECPhoneNumberFormatter.h"

@interface IKTGalleryDetailsViewController ()

@end

@implementation IKTGalleryDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [_detailsActivityIndicator setHidden:NO];
    [_scrollView setHidden:YES];
    
    LibraryAPI *library = [LibraryAPI sharedInstance];
    [library setDelegate:self];
    [library loadGalleryDetailsWithPlaceId:[_details placeId]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Gallery Details Screen";
}

- (void) updateView {
    [_countryLabel setText:[_details country]];
    [_titleLabel setText:[_details name]];
    [_cityLabel setText:[_details city]];
    [_addressButton setTitle:[_details address] forState:UIControlStateNormal];
    [_phoneButton setTitle:[_details phone] forState:UIControlStateNormal];
    [_visitingHoursTableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_details visitingHours] count];
}

- (IKTVisitingHoursTableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    IKTVisitingHoursTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[IKTVisitingHoursTableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellIdentifier];
    }
    
    if ([[_details visitingHours] count]) {
        IKTVisitingPeriod *period = [[_details visitingHours] objectAtIndex:indexPath.row];
        NSDateFormatter * df = [[NSDateFormatter alloc] init];
        [df setLocale: [NSLocale currentLocale]];
        NSArray *weekdays = [df weekdaySymbols];
        
        NSMutableString *open = [[period open] mutableCopy];
        NSMutableString *close = [[period close] mutableCopy];
        [open insertString:@":" atIndex:2];
        [close insertString:@":" atIndex:2];

        [cell.dayLabel setText:[weekdays objectAtIndex:[period day]]];
        [cell.openLabel setText:open];
        [cell.closeLabel setText:close];
    }
    return cell;
}

- (IBAction)phoneButtonTapped:(id)sender {
    ECPhoneNumberFormatter *formatter = [[ECPhoneNumberFormatter alloc] init];
    NSString *phone;
    NSString *error;
    [formatter getObjectValue:&phone forString:[_details phone] errorDescription:&error];

    NSURL *phoneUrl = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"tel://%@", phone]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    } else {
        UIAlertView *calert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Call option is not available!"
                                                        delegate:nil
                                               cancelButtonTitle:@"ok"
                                               otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)addressButtonTapped:(id)sender {
    NSString *address = [[_details address] stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com/?q=%@", [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    } else {
        UIAlertView *calert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:[NSString stringWithFormat:@"Sorry, not able to open \n%@ \naddress in Maps!", [_details address]]
                                                        delegate:nil
                                               cancelButtonTitle:@"ok"
                                               otherButtonTitles:nil, nil];
        [calert show];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - LibraryAPI Delegate
- (void) libraryDidLoadGalleryDetails: (id) galleryDetails {
    _details = galleryDetails;
    [self updateView];
    
    [_scrollView setHidden:NO];
    [_detailsActivityIndicator setHidden:YES];
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}

@end
