//
//  ArtworkView.h
//  iKollect
//
//  Created by mihata on 5/14/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArtworkDraggableView.h"


@interface ArtworkView : UIView
@property (nonatomic, strong) ArtworkDraggableView *draggableView;
@property (nonatomic, strong) NSString *draggableImage;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *gallery;
@property (nonatomic, strong) NSString *dimensions;



-(void) loadDraggableCustomView;
@end
