//
//  IKTVisitingHoursTableViewCell.swift
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/16/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

import UIKit

class IKTVisitingHoursTableViewCell: UITableViewCell {
   
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var openLabel: UILabel!
    @IBOutlet weak var closeLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
