//
//  Gallery.m
//  iKollect
//
//  Created by mihata on 7/24/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "Gallery.h"

@implementation Gallery

- (id) initWithId:(NSString *)uid
             name:(NSString *)name
          country:(NSString *)country
             city:(NSString *)city
          address:(NSString *)address
         postcode:(NSString *)postcode
          placeId:(NSString *)placeId
            phone:(NSString *)phone
           rating:(NSString *)rating
      andLocation:(CLLocation *)location
{
    self = [super init];
    if (self) {
        _uid = uid;
        _name = name;
        _country = country;
        _city = city;
        _address = address;
        _postcode = postcode;
        _location = location;
        _placeId = placeId;
        _phone = phone;
        _rating = rating;
    }
    
    return self;
}



- (id) init {
    self = [super init];
    if (self) {
        _uid = @"";
        _name = @"";
        _country = @"";
        _city = @"";
        _address = @"";
        _postcode = @"";
        _placeId = @"";
        _phone = @"";
        _rating = @"";
    }
    return self;
}
@end
