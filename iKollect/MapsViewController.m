//
//  MapsViewController.m
//  iKollect
//
//  Created by mihata on 7/23/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "MapsViewController.h"
#import "GalleryLocation.h"
#import "Gallery.h"
#import "SWRevealViewController.h"
#import "UserAnnotation.h"
#import "IKTGalleryDetailsViewController.h"

@interface MapsViewController () {

    CLLocationManager* locationManager;
    CLLocation* currentLocation;
    NSArray* galleries;
}

@end

@implementation MapsViewController

@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *sidebarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = sidebarButton;
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [LibraryAPI sharedInstance].delegate = self;
    
    [self refreshMapView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Maps Screen";
}

- (void) refreshMapView {
    NSMutableArray * annotationsToRemove = [ mapView.annotations mutableCopy ] ;
    [ annotationsToRemove removeObject:mapView.userLocation ] ;
    [ mapView removeAnnotations:annotationsToRemove ] ;
    
    UserSettings *userSettings = [[User sharedInstance] settings];
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = userSettings.latitude;
    zoomLocation.longitude = userSettings.longitude; //
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, METERS_PER_MILE, METERS_PER_MILE);
    
    [mapView setShowsPointsOfInterest:NO];
    
    UserAnnotation *annotation = [UserAnnotation new];
    annotation.coordinate = zoomLocation;
    annotation.title = [[User sharedInstance] name];
    
    [mapView addAnnotation:annotation];
    
    [mapView setRegion:viewRegion animated:YES];
    [locationManager stopUpdatingLocation];
    
    [[LibraryAPI sharedInstance] loadGalleries];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Implementing LibraryAPIDelegate
- (void) libraryDidLoadGalleries
{
    galleries = [[NSArray alloc] initWithArray:[[LibraryAPI sharedInstance] getGalleries]];
    
    CLLocationCoordinate2D coordinate;
    for (Gallery *gallery in galleries) {
        coordinate.latitude = gallery.location.coordinate.latitude;
        coordinate.longitude = gallery.location.coordinate.longitude;
        
        GalleryLocation *annotation = [[GalleryLocation alloc] initWithTitle:[gallery name]
                                                                 andSubtitle:[gallery address]
                                                               andCoordinate:coordinate];
        [annotation setPlaceId:[gallery placeId]];
        
        [mapView addAnnotation:annotation];
    }
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}

#pragma mark Implementing MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapInstance viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"GalleryLocation";
    if ([annotation isKindOfClass:[UserAnnotation class]]) {
        identifier = @"UserLocation";
        MKAnnotationView *annotationView = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:identifier];
            annotationView.enabled = YES;

            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"user.png"];//here we use a nice image instead of the default pins
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    } else if ([annotation isKindOfClass:[GalleryLocation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"museum.png"];
            annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    // Go to edit view
//    IKTGalleryDetailsViewController *detailViewController = [[IKTGalleryDetailsViewController alloc] initWithNibName:@"IKTGalleryDetailsViewController" bundle:nil];
//    [self.navigationController pushViewController:detailViewController animated:YES];
    GalleryLocation *location = (GalleryLocation *) view.annotation;
    [self performSegueWithIdentifier:@"showGalleryDetails" sender:location  ];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    IKTGalleryDetailsViewController *detailsController = [segue destinationViewController];
    detailsController.details = [[IKTGalleryDetails alloc] init];
    [detailsController.details setPlaceId:[sender placeId]];
}


@end
