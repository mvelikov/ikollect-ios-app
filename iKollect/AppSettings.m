//
//  AppSettings.m
//  iKollect
//
//  Created by mihata on 9/26/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "AppSettings.h"

@implementation AppSettings

- (id) init {
    if (self = [super init]) {
        _cityNames = [[NSMutableArray alloc] initWithArray:@[@"Please select"]];
        
        _cityLocations = [[NSMutableDictionary alloc] init];
        _cityLocations = [@{
                          @"Washington, DC" : @{@"lat" : @38.907286, @"lng" : @-77.036537, @"name" : @"Washington, DC"},
                          } mutableCopy];
        
        for (id city in _cityLocations) {
            [_cityNames addObject:city];
        }
        
        _artworkCategories = [[NSMutableDictionary alloc] init];
        _artworkCategories = [@{
                              @"7" : @"Museums & Exhibitions",
                              @"8" : @"Galleries",
                              @"9" : @"Independent Artists"
                              } mutableCopy];
    }
    return self;
}

+ (AppSettings*) sharedInstance
{
    // 1
    static AppSettings *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[AppSettings alloc] init];
    });
    return _sharedInstance;
}

- (id) initWithCities: (NSDictionary *) cities AndCategories: (NSDictionary *) categories
{
    if (self = [super init]) {
        _cityNames = [[NSMutableArray alloc] initWithArray:@[@"Please select"]];
        _cityLocations = [cities mutableCopy];
        
        for (id city in _cityLocations) {
            [_cityNames addObject:city];
        }
        _artworkCategories = [categories mutableCopy];
    }
    return self;
}

@end
