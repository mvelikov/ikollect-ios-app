//
//  UserSettings.m
//  iKollect
//
//  Created by mihata on 9/5/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "UserSettings.h"

@implementation UserSettings

- (id) init
{
    if (self = [super init]) {
        _address = @"";
        _city = @"Washington, DC";
        _locationType = @"";
        _searchFor = @"";

        // Trafalgar Square
        _latitude = 38.907286;
        _longitude = -77.036537;

        _distance = 10;
        
        _categories = [[NSMutableDictionary alloc] initWithDictionary: @{
                                                                     @"7" : @"Museums & Exhibitions",
                                                                     @"8" : @"Galleries",
                                                                     @"9" : @"Independent Artists"
                                                                     }];
    }
    return self;
}
#pragma mark NSCoding protocol
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_city forKey:@"city"];
    [encoder encodeObject:_address forKey:@"address"];
    [encoder encodeObject:_locationType forKey:@"locationType"];
    [encoder encodeObject:_searchFor forKey:@"searchFor"];
    
    [encoder encodeFloat:_latitude forKey:@"latitude"];
    [encoder encodeFloat:_longitude forKey:@"longitude"];
    [encoder encodeInt:_distance forKey:@"distance"];
    [encoder encodeObject:_categories forKey:@"categories"];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _city = [decoder decodeObjectForKey:@"city"];
    _address = [decoder decodeObjectForKey:@"address"];
    _locationType = [decoder decodeObjectForKey:@"locationType"];
    _searchFor = [decoder decodeObjectForKey:@"searchFor"];
    
    _latitude = [decoder decodeFloatForKey:@"latitude"];
    _longitude = [decoder decodeFloatForKey:@"longitude"];
    _distance = (int) [decoder decodeIntegerForKey:@"distance"];
    _categories = [decoder decodeObjectForKey:@"categories"];
    
    return self;
}

@end
