//
//  HTTPClient.m
//  iKollect
//
//  Created by mihata on 5/19/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "HTTPClient.h"
#import "AFHTTPRequestOperationManager.h"

@implementation HTTPClient {
    AFHTTPRequestOperationManager *manager;
}

- (id) init {
    if (self = [super init]) {
        manager = [AFHTTPRequestOperationManager manager];
    }
    
    return self;
}
- (id) getRequest:(NSString *)url
{

    NSString *fullUrl = [NSString stringWithFormat:@"%@%@", API_URL, url];
    
    [manager GET:fullUrl
      parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject){
            id<HTTPClientDelegate> strongDelegate = self.delegate;
            
            if ([strongDelegate respondsToSelector:@selector(loadDataFromServerWithResponse:)]) {
                [strongDelegate loadDataFromServerWithResponse:responseObject];
            }
    }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {

    }];

    return self;
}

- (id) getArtworksForUser:(NSString *)user
{
    return [self getArtworksForUser:user atLatitude:0.0 AndLongitude:0.0];
}

- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude InDistance: (int) distance
{
    return [self getArtworksForUser:user atLatitude:latitude AndLongitude:longitude InDistance:distance AtPage:0];
}

- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude InDistance: (int) distance AtPage: (int) page
{
    return [self getArtworksForUser:user atLatitude:latitude AndLongitude:longitude InDistance:distance ForCategories:@"" AtPage:distance];
}

- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude InDistance: (int) distance ForCategories: (NSString *)categories AtPage: (int) page
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/%@/%@/%.6f/%.6f/%@/%d",
                         API_URL,
                         @"artworks",
                         user,
                         latitude,
                         longitude,
                         categories,
                         distance
                         ];
    
    [manager GET:fullUrl
      parameters:@{@"page" : [[NSNumber alloc] initWithInt:page]}
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             id<HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(loadDataFromServerWithResponse:)]) {
                 [strongDelegate loadDataFromServerWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                 [strongDelegate showErrorViewWithTitle:@"Unable to Load Artworks" andError:error];
             }
         }];
    
    
    return self;
}

- (id) getArtworksForUser:(NSString *)user atLatitude:(float)latitude AndLongitude:(float)longitude
{
    [self getArtworksForUser:user atLatitude:latitude AndLongitude:longitude InDistance:10];
    return self;
}

- (id) getGalleriesNearLatitude: (float)latitude AndLongitude: (float) longitude AndDistance: (int)distance forUser: (NSString * ) user
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",
                         API_URL,
                         @"galleries",
                         [NSString stringWithFormat:@"%.6f", latitude ],
                         [NSString stringWithFormat:@"%.6f", longitude],
                         [NSString stringWithFormat:@"%d", distance]
                         ];
    [manager GET:fullUrl
      parameters:@{@"user": user}
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             id<HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(loadGalleriesFromServerWithResponse:)]) {
                 [strongDelegate loadGalleriesFromServerWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                 [strongDelegate showErrorViewWithTitle:@"Unable to Load Galleries" andError:error];
             }
         }];
    return self;

}
- (id) getGalleriesNearLatitude: (float)latitude AndLongitude: (float) longitude AndDistance: (int)distance
{
    return [self getGalleriesNearLatitude:latitude AndLongitude:longitude AndDistance:distance forUser:@"0"];
}

- (id) getTagsForUser:(int)userId AtPage: (int) page
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/tags/%d/10",
                         API_URL,
                         userId];
    [manager GET:fullUrl
      parameters:@{@"page": [NSString stringWithFormat:@"%d", page]}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id<HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(loadTagsFromServerWithResponse:)]) {
                 [strongDelegate loadTagsFromServerWithResponse:responseObject];
             }
    }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                 [strongDelegate showErrorViewWithTitle:@"Unable to Load Tags" andError:error];
             }
    }];
    
    return self;
}

- (void) sendVoteRequestForUser : (NSString*) userId andArtwork: (NSString*) artworkId withVote:(NSInteger)vote atLatitude: (float) latitude andLongitude: (float) longitude {
    NSString *fullUrl = [NSString stringWithFormat:@"%@/vote/%@/%@/%ld",
                         API_URL,
                         userId,
                         artworkId,
                         (long)vote];
    
    [manager GET:fullUrl
      parameters:@{@"latitude" : [NSString stringWithFormat:@"%.6f", latitude],
                   @"longitude": [NSString stringWithFormat:@"%.6f", longitude]}
         success:^(AFHTTPRequestOperation *operation, id responseObject){
             // NSLog(@"success");
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             // NSLog(@"failure");
         }];
}
- (void) sendVoteRequestForUser : (NSString*) userId andArtwork: (NSString*) artworkId withVote:(NSInteger)vote
{
    [self sendVoteRequestForUser:userId
                      andArtwork:artworkId
                        withVote:vote
                      atLatitude:0.0
                    andLongitude:0.0];
}

- (void) getGeoLocationForAddress: (NSString*)address
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/getgeolocation/%@", API_URL, address];
    
    [manager GET:fullUrl
      parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
            id <HTTPClientDelegate> strongDelegate = self.delegate;
            
            if ([strongDelegate respondsToSelector:@selector(geoLocationForAddressWithResponse:)]) {
                [strongDelegate geoLocationForAddressWithResponse:responseObject];
            }
    
    }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            id <HTTPClientDelegate> strongDelegate = self.delegate;
            
            if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                [strongDelegate showErrorViewWithTitle:@"Unable to Provide Address Geolocation" andError:error];
            }
    }];
}

- (void) loginUserWithEmail: (NSString*) email andPassword: (NSString*) password
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/login",
                         API_URL];
    [manager POST:fullUrl
       parameters:@{@"email": email, @"password" : password}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              // NSLog(@"success");
              id <HTTPClientDelegate> strongDelegate = self.delegate;
              if ([strongDelegate respondsToSelector:@selector(doLoginUserWithResponse:)]) {
                  [strongDelegate doLoginUserWithResponse:responseObject];
              }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              id <HTTPClientDelegate> strongDelegate = self.delegate;
              
              if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                  [strongDelegate showErrorViewWithTitle:@"Unable to Log In" andError:error];
              }
    }];
}

- (void) createCustomerProfileForFacebookUser:(id)user
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/signup",
                         API_URL];
    [manager POST:fullUrl
       parameters:@{
                    @"email" : [user valueForKey:@"email"],
                    @"gender" : [user valueForKey:@"gender"],
                    @"fullname" : [user valueForKey:@"name"],
                    @"username" : [user valueForKey:@"id"]}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              // NSLog(@"success %@", responseObject);
              id <HTTPClientDelegate> strongDelegate = self.delegate;
              
              if ([strongDelegate respondsToSelector:@selector(doLoginUserWithResponse:)]) {
                  [strongDelegate doLoginUserWithResponse:responseObject];
              }
       }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              id <HTTPClientDelegate> strongDelegate = self.delegate;
              
              if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                  [strongDelegate showErrorViewWithTitle:@"Unable to Sign In" andError:error];
              }
       }];
}

- (void) loadAppSettings
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/settings",
                         API_URL];
    
    [manager GET:fullUrl
      parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             // NSLog(@"success %@", responseObject);
             NSMutableDictionary *settings = [responseObject mutableCopy];
                          
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(didLoadAppSettings:)]) {
                 [strongDelegate didLoadAppSettings:settings];
             }

    }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                 [strongDelegate showErrorViewWithTitle:@"Unable to Initialize the App" andError:error];
             }
    }];
}

- (void) loadBucketsListForUser: (NSString *) user atPage: (int) page {
    NSString *fullUrl = [NSString stringWithFormat:@"%@/buckets/all/%@",
                         API_URL,
                         user];
    
    [manager GET:fullUrl
      parameters:@{@"page" : [NSString stringWithFormat:@"%d", page]}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(didLoadBucketsWithResponse:)]) {
                 [strongDelegate didLoadBucketsWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
                 [strongDelegate showErrorViewWithTitle:@"Unable to Load List of Buckets" andError:error];
             }
         }];
}

- (void) createBucketWithName:(NSString*) name forUser: (NSString *) user
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/buckets/create/%@",
                         API_URL,
                         user];
    [manager POST:fullUrl parameters:@{@"name" : name}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              id <HTTPClientDelegate> strongDelegate = self.delegate;
              
              if ([strongDelegate respondsToSelector:@selector(didCreateBucketWithResponse:)]) {
                  [strongDelegate didCreateBucketWithResponse:responseObject];
              }

          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [self respondToErrorWithTitle:@"Unable to Create Bucket" andError:error];
          }];

}
- (void) addArtwork:(NSString*) artwork toBuckets: (NSString *) buckets
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/buckets/add",
                         API_URL];
    
    [manager POST:fullUrl parameters:@{ @"artwork" : artwork,
                                        @"buckets" : buckets}
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              id <HTTPClientDelegate> strongDelegate = self.delegate;
              
              if ([strongDelegate respondsToSelector:@selector(didFinishAddingArtworkInBucketsWithResponse:)]) {
                  [strongDelegate didFinishAddingArtworkInBucketsWithResponse:responseObject];
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              [self respondToErrorWithTitle:@"Unable to Add Artwork to Buckets" andError:error];
          }];
}
- (void) loadArtworksFromBucket: (NSString *)bucket atPage: (NSUInteger) page
{
    NSString *fullUrl = [NSString stringWithFormat:@"%@/buckets/artworks/%@",
                         API_URL,
                         bucket];
    
    [manager GET:fullUrl parameters:@{@"page" : [NSString stringWithFormat:@"%lu", (unsigned long)page ]}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(didLoadBucketedArtworksWithResponse:)]) {
                 [strongDelegate didLoadBucketedArtworksWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [self respondToErrorWithTitle:@"Unable to Load Artworks from Selected Bucket" andError:error];
    }];
}

- (void) loadArtworkWithId: (NSString *)uid forUser: (NSString*) user atLatitude: (float) latitude andLongitude: (float) longitude {
    NSString *fullUrl = [NSString stringWithFormat:@"%@/artworks/get/%@",
                         API_URL,
                         uid];
    [manager GET:fullUrl
      parameters:@{
                   @"user" : user,
                   @"latitude" : [NSString stringWithFormat:@"%.6f", latitude],
                   @"longitude" : [NSString stringWithFormat:@"%.6f", longitude]}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(didLoadArtworkWithResponse:)]) {
                 [strongDelegate didLoadArtworkWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [self respondToErrorWithTitle:@"Unable to Load Artwork" andError:error];
         }];

}
- (void) loadArtworkWithId: (NSString *)uid {
    [self loadArtworkWithId:uid
                    forUser:@"0"
                 atLatitude:0.0
               andLongitude:0.0];
}

- (void) loadLikedArtworksForUser: (NSString *) user atPage: (NSUInteger) page {
    NSString *fullUrl = [NSString stringWithFormat:@"%@/artworks/liked/%@",
                         API_URL,
                         user];
    
    [manager GET:fullUrl
      parameters:@{@"page" : [NSString stringWithFormat:@"%lu", (unsigned long)page]}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(didLoadBucketedArtworksWithResponse:)]) {
                 [strongDelegate didLoadBucketedArtworksWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [self respondToErrorWithTitle:@"Unable to Load Liked Artworks" andError:error];
    }];
}

- (void) loadGalleryDetailsWithPlaceId:(NSString*) placeId {
    NSString *fullUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",
                         placeId,
                         GOOGLE_PLACE_API_KEY];
    
    [manager GET:fullUrl
      parameters:@{}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id <HTTPClientDelegate> strongDelegate = self.delegate;
             
             if ([strongDelegate respondsToSelector:@selector(didLoadGalleryDetailsWithResponse:)]) {
                 [strongDelegate didLoadGalleryDetailsWithResponse:responseObject];
             }
         }
         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [self respondToErrorWithTitle:@"Unable to Load Gallery Details" andError:error];
         }];
}

- (void) respondToErrorWithTitle:(NSString*)title andError:(NSError *)error {
    id <HTTPClientDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
        [strongDelegate showErrorViewWithTitle:title andError:error];
    }
    
}


@end
