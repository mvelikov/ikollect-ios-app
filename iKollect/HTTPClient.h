//
//  HTTPClient.h
//  iKollect
//
//  Created by mihata on 5/19/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol HTTPClientDelegate;

@interface HTTPClient : NSObject

@property (nonatomic, weak) id <HTTPClientDelegate> delegate;
//- (id) getRequest:(NSString*)url;
//- (id) postRequest:(NSString*)url body:(NSString*)body;
//- (void) sendRequest:(NSString*)url body:(NSString*)body;
- (void) sendVoteRequestForUser : (NSString*) userId andArtwork: (NSString*) artworkId withVote:(NSInteger)vote;
- (void) sendVoteRequestForUser : (NSString*) userId andArtwork: (NSString*) artworkId withVote:(NSInteger)vote atLatitude: (float) latitude andLongitude: (float) longitude;
- (id) getArtworksForUser:(NSString *)user;
- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude;
- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude InDistance: (int) distance;
- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude InDistance: (int) distance AtPage: (int) page;
- (id) getArtworksForUser:(NSString *)user atLatitude: (float)latitude AndLongitude: (float) longitude InDistance: (int) distance ForCategories: (NSString *)categories AtPage: (int) page;

- (id) getGalleriesNearLatitude: (float)latitude AndLongitude: (float) longitude AndDistance: (int)distance;
- (id) getGalleriesNearLatitude: (float)latitude AndLongitude: (float) longitude AndDistance: (int)distance forUser: (NSString * ) user;
- (void) getGeoLocationForAddress: (NSString*)address;
- (void) loginUserWithEmail: (NSString*) email andPassword: (NSString*) password;
- (void) createCustomerProfileForFacebookUser:(id)user;

- (id) getTagsForUser:(int)userId AtPage: (int) page;

- (void) loadAppSettings;

- (void) loadBucketsListForUser: (NSString *) user atPage: (int) page;
- (void) createBucketWithName:(NSString*) name forUser: (NSString *) user;
- (void) addArtwork:(NSString*) artwork toBuckets: (NSString *) buckets;

- (void) loadArtworksFromBucket: (NSString *)bucket atPage: (NSUInteger) page;

- (void) loadArtworkWithId: (NSString *)uid;
- (void) loadArtworkWithId: (NSString *)uid forUser: (NSString*) user atLatitude: (float) latitude andLongitude: (float) longitude;

- (void) loadLikedArtworksForUser: (NSString *) user atPage: (NSUInteger) page;
- (void) loadGalleryDetailsWithPlaceId:(NSString*) placeId;
@end

@protocol HTTPClientDelegate <NSObject>

@optional

- (void) loadGalleriesFromServerWithResponse:(id) responseObject;
- (void) loadDataFromServerWithResponse:(id) responseObject;
- (void) loadTagsFromServerWithResponse:(id) responseObject;
- (void) geoLocationForAddressWithResponse:(id) responseObject;
- (void) showErrorViewWithTitle:(NSString*) title andError:(NSError*)error;
- (void) doLoginUserWithResponse:(id) responseObject;
- (void) doLoginFacebookUserWithResponse:(id) responseObject;
- (void) didLoadAppSettings:(NSDictionary *) settings;

- (void) didLoadBucketsWithResponse: (id) responseObject;
- (void) didCreateBucketWithResponse: (id) responseObject;
- (void) didFinishAddingArtworkInBucketsWithResponse: (id) responseObject;
- (void) didLoadBucketedArtworksWithResponse: (id) responseObject;

- (void) didLoadArtworkWithResponse: (id) responseObject;
- (void) didLoadLikedArtworksWithResponse: (id) responseObject;

- (void) didLoadGalleryDetailsWithResponse: (id) responseObject;
@end
