//
//  ArtworkOverlayView.m
//  iKollect
//
//  Created by mihata on 5/14/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "ArtworkOverlayView.h"

@interface ArtworkOverlayView ()
@property (nonatomic, strong) UIImageView *imageView;
@end

@implementation ArtworkOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor whiteColor];
        self.imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"no-big"]];
        [self addSubview:self.imageView];

    }
    return self;
}


- (void)setMode:(ArtworkOverlayViewMode)mode
{
    if (_mode == mode) return;
    
    _mode = mode;
    if (mode == ArtworkViewModeLeft) {
        self.imageView.image = [UIImage imageNamed:@"no-big"];
    } else {
        self.imageView.image = [UIImage imageNamed:@"yes-big"];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(84, 48, 160, 160);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
