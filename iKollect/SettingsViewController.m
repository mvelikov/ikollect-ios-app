//
//  SettingsViewController.m
//  iKollect
//
//  Created by mihata on 7/2/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "SettingsViewController.h"
#import "SWRevealViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SettingsViewController () {
    NSMutableArray* cityNames;
    NSDictionary *artworkCategories;
    NSDictionary *cityLocations;
    BOOL isAddressPopupShown, isCityPickerShown;
    CLLocationManager *locationManager;
    NSTimer *timer;
}

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    AppSettings *appSettings = [AppSettings sharedInstance];
    cityNames = [appSettings cityNames];
    cityLocations = [appSettings cityLocations];

    artworkCategories = [appSettings artworkCategories];
    
    
    isCityPickerShown = isAddressPopupShown = NO;
    

    User *user = [User sharedInstance];
    UserSettings *userSettings = [user settings];
    

    [self setLocationFieldsValuesForUserSettings:userSettings];
    [self setAccountFieldsValuesForUser:user];
    [self setSearchForFieldsValuesForUserSettings:userSettings];
     
    long defaultDistance = [userSettings distance];
    if (defaultDistance) {
        _distanceSlider.value = defaultDistance;
        _distanceSliderLabel.text = [NSString stringWithFormat:@"%ld", defaultDistance];
    }
    
    _cityField.delegate = self;
    _addressField.delegate = self;

    LibraryAPI *library = [LibraryAPI sharedInstance];
    library.delegate = self;
    
    [library loadTagsPage:1];
    [_tagsLoadingIndicator setHidden:NO];

    
    UIBarButtonItem *sidebarButton = [[Helper sharedInstance] createMainMenuButtonAtNavigationItemWithTarge:self AndAction:@selector(sidebarButtonTapped:)];


    self.navigationItem.leftBarButtonItem = sidebarButton;
    self.revealViewController.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(doneButton:)];
    
    [self.view addGestureRecognizer:tap];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.screenName = @"Settings Screen";
}

- (void) setSearchForFieldsValuesForUserSettings: (UserSettings *) userSettings {
    NSDictionary *categories = [userSettings categories];
    
    if ([categories objectForKey:@"7"] == nil) {
        [_museumsSwitch setOn:NO];
    }
    if ([categories objectForKey:@"8"] == nil) {
        [_galleriesSwitch setOn:NO];
    }
    if ([categories objectForKey:@"9"] == nil) {
        [_artistsSwitch setOn:NO];
    }
}

- (void) setAccountFieldsValuesForUser:(User *) user {
    [_accountAvatar sd_setImageWithURL:[user avatar] placeholderImage:[UIImage imageNamed:@"user_blank"]];
    [_accountLabel setText:[user name]];
    
    NSString *loginType = [user loginType];
    if ([loginType isEqualToString:LOGIN_TYPE_FACEBOOK]) {
        _facebookLoginView.delegate = self;
        [_facebookLoginView setHidden:NO];
    } else if ([loginType isEqualToString:LOGIN_TYPE_EMAIL]) {
        [_logoutButton setHidden:NO];
    }
}

- (void) setLocationFieldsValuesForUserSettings: (UserSettings *) userSettings
{
    NSString *defaultUserCity = [userSettings city];
    NSString *defaultUserAddress = [userSettings address];
    if (defaultUserCity != nil) {
        if ([defaultUserCity isEqualToString:@"Custom"]) {
            _addressField.text = defaultUserAddress;
            [_cityField setAlpha:0.5];
            [_geoLocationSwitch setAlpha:0.5];
            [_addressField setAlpha:1];
        } else if ([defaultUserCity isEqualToString:@"geoLocation"]) {
            [_cityField setAlpha:0.5];
            [_geoLocationSwitch setAlpha:1];
            [_addressField setAlpha:0.5];
            
            [_geoLocationSwitch setOn:YES animated:YES];
        } else {
            _cityField.text = defaultUserCity;
            [_cityField setAlpha:1];
            [_geoLocationSwitch setAlpha:0.5];
            [_addressField setAlpha:0.5];
        }
    }
}

- (IBAction)sidebarButtonTapped:(id)sender
{
    [self saveUserDistance];
    [self.revealViewController revealToggle:sender];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController
{
    // NSLog(@"revealControllerPanGestureShouldBegin");
    [self saveUserDistance];
    return YES;
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    // NSLog(@"revealController");
    
}

- (void) saveUserDistance
{
    [[[User sharedInstance] settings] setDistance:(int)_distanceSlider.value];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[User sharedInstance] saveUserSettings];
    });
}

- (void) startStandardLocationService {

    [[Helper sharedInstance] startStandardLocationService];
    
//    [_geoLocationIndicator setHidden:NO];
}

-(void) logoutUser
{
    [[User sharedInstance] clearUser];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"isInstructionsScreenShown"];
    [defaults synchronize];
    
    [self performSegueWithIdentifier:@"pushRootViewController" sender:self];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[User sharedInstance] saveUser];
    });
    
}

- (IBAction)logoutButtonTapped:(id)sender
{
    [self logoutUser];
}

- (IBAction)switchValueChanged:(UISwitch*)sender
{
    [self.view endEditing:YES];
    [self hideCustomCityPicker];
    
    [_addressField setAlpha:0.5];
    if (sender.isOn) {
        [_cityField setAlpha:0.5];
        [_geoLocationSwitch setAlpha:1];
        
        [self startStandardLocationService];
    } else {
        [_cityField setAlpha:1];
        [_geoLocationSwitch setAlpha:0.5];
        
        [[Helper sharedInstance] stopUpdatingLocation];
        [self saveCityAfterPickerHides];
    }
}

- (IBAction)searchForSwitchesValueChanged:(UISwitch *)sender {
    NSString *key = @"";
    
    if (sender == _museumsSwitch) {
        key = @"7";
    } else if (sender == _galleriesSwitch) {
        key = @"8";
    } else if (sender == _artistsSwitch) {
        key = @"9";
    }
    
    NSMutableDictionary *categories = [[[User sharedInstance] settings] categories];
    if (sender.isOn) {
        id object = [artworkCategories objectForKey:key];
        [categories setObject:object forKey:key];
    } else {
        [categories removeObjectForKey:key];
    }
}

- (void) saveCityAfterPickerHides
{
    NSString *city = _cityField.text;
    
    float latitude = [[[cityLocations objectForKey:city] objectForKey:@"lat"] floatValue];
    float longitude = [[[cityLocations objectForKey:city] objectForKey:@"lng"] floatValue];
    
    //    CLLocation *currentLocation = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    //    [[User sharedInstance] setCurrentLocation:currentLocation];
    UserSettings *userSettings = [[User sharedInstance] settings];
    [userSettings setLatitude:latitude];
    [userSettings setLongitude:longitude];
    [userSettings setCity:city];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[User sharedInstance] saveUserSettings];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManagerDelegate protocol implementation
-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
//    Helper *helper = [Helper sharedInstance];
//    [helper locationManager:manager didUpdateLocations:locations];
//    [helper stopUpdatingLocation];
    
    [_geoLocationIndicator setHidden:YES];
    [_successfullGeoLocationIndicator setHidden:NO];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(hideSuccessFullGeoLocationIndicator) userInfo:nil repeats:NO];
}

- (void) hideSuccessFullGeoLocationIndicator{
    [_successfullGeoLocationIndicator setHidden:YES];
}
- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {

//    [[Helper sharedInstance] locationManager:manager didFailWithError:error];

    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        [_geoLocationIndicator setHidden:YES];
        [_geoLocationSwitch setOn:NO animated:YES];
    }

}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return cityNames.count;
 
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return cityNames[row];
}


#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    NSString *resultString = [NSString stringWithFormat:@"%@", cityNames[row]];
    
    if (![resultString isEqualToString:@"Please select"])
        _cityField.text = resultString;

    [self saveCityAfterPickerHides];
    
}

#pragma mark TextField Delegate

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    [self hideCustomCityPicker];
    
    [self.view endEditing:YES];
    if (textField == _cityField) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        CGRect frame = CGRectMake(0, self.view.frame.size.height - 207, self.view.frame.size.width, 207);
        
        _cityPickerViewContainer.frame = frame;

        [UIView commitAnimations];
        
        [_cityField setAlpha:1];
        [_geoLocationSwitch setAlpha:0.5];
        [_addressField setAlpha:0.5];
        [_geoLocationSwitch setOn:NO animated:YES];
        
        isCityPickerShown = YES;

    } else if (textField == _addressField && isAddressPopupShown == NO) {
        UIAlertView *alertView = [[UIAlertView alloc ] initWithTitle:@"Desired Address"
                                                             message:@"Enter the address where you want to start your search from."
                                                            delegate:nil
                                                   cancelButtonTitle:@"Cancel"
                                                   otherButtonTitles:@"OK", nil];
        isAddressPopupShown = YES;
        
        alertView.delegate = self;

        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        [[alertView textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
        if (![_addressField.text isEqualToString:@""]) {
            UITextField * alertTextField = [alertView textFieldAtIndex:0];
            alertTextField.text = _addressField.text;
        }
        
        [alertView show];
        [_cityField setAlpha:0.5];
        [_geoLocationSwitch setAlpha:0.5];
        [_addressField setAlpha:1];
        [_geoLocationSwitch setOn:NO animated:YES];
    }
    return NO;
}


- (IBAction)doneButton:(id)sender {
    if (isCityPickerShown == YES) {
        [self hideCustomCityPicker];
        
        [self saveCityAfterPickerHides];
    }
}

-(void) hideCustomCityPicker {
    CGRect frame = CGRectMake(0, self.view.frame.size.height , self.view.frame.size.width, 207);
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    _cityPickerViewContainer.frame = frame;
    
    [UIView commitAnimations];
    
    isCityPickerShown = NO;
}

#pragma mark - LibraryAPI Delegate Methods

- (void)loadAddressWithLatitude:(float)latitude AndLongitude:(float) longitude {

    UserSettings *userSettings = [[User sharedInstance] settings];
    
    [userSettings setLocationType:@"Custom"];
    [userSettings setCity:@"Custom"];
    [userSettings setAddress:_addressField.text];
    [userSettings setLatitude:latitude];
    [userSettings setLongitude:longitude];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[User sharedInstance] saveUserSettings];
    });
    
    [_addressIndicator setHidden:YES];
}

- (void) libraryDidLoadTags
{
    int height = 21, width = 140, x = 20, y = 20, counter = 1;
    UILabel *label;
//    UIColor *tintColor = [[[UIApplication sharedApplication] delegate] window].tintColor;
    for (Tag *tag in [[LibraryAPI sharedInstance] getTags]) {
        // NSLog(@"%@", tag);
        CGRect frame = CGRectMake(x, y, width, height);

        label = [[UILabel alloc] initWithFrame:frame];
//        [label setTextColor:tintColor];
        [label setText: [tag name]];
        [_tagsLabesWrapperView addSubview:label];
        
        if (counter % 2 == 1) {
            x += width;
//            y = 20;
        } else {
            y += 29;
            x = 20;
        }
        counter++;
    }
    [_tagsLoadingIndicator setHidden:YES];
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
    
}

#pragma mark - Alert View Delegate Methods

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    UITextField *addressTextField = [alertView textFieldAtIndex:0];

    if (buttonIndex == 1 && ![addressTextField.text isEqualToString:@""]) { // OK button tapped

        _addressField.text = addressTextField.text;
        [_addressIndicator setHidden:NO];
        [[LibraryAPI sharedInstance] getGeoLocationForAddress:_addressField.text];
    }
}

- (void)didPresentAlertView:(UIAlertView *)alertView {
    if (isAddressPopupShown) {

        [_addressField becomeFirstResponder];
    }
    isAddressPopupShown = NO;
}

#pragma mark - Distance Slider
- (IBAction)distanceSliderValueChanged:(id)sender
{
    NSString *labelString = [NSString stringWithFormat:@"%d", (int)_distanceSlider.value];
    [_distanceSliderLabel setText:labelString];
}

- (IBAction)distanceSliderDragExit:(id)sender
{
    [[[User sharedInstance] settings] setDistance:(int)_distanceSlider.value];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[User sharedInstance] saveUserSettings];
    });
}

#pragma mark - FacebookAPIDelegate

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    [self logoutUser];

    // NSLog(@"You're logged out");
}
@end
