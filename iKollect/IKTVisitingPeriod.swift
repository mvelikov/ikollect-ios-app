//
//  IKTVisitingPeriod.swift
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/16/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

import UIKit

class IKTVisitingPeriod: NSObject {
   
    var day: Int = -1
    var open: String = "00:00"
    var close: String = "00:00"
    
    override init() {
        super.init()
    }
    
    init(day: Int, open: String, close: String) {
        self.day = day
        self.open = open
        self.close = close
    }
}
