//
//  IKTBucketsTableViewController.m
//  IKOLLECT
//
//  Created by mihata on 10/7/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTBucketsTableViewController.h"
#import "IKTArtworksTableViewController.h"
#import "SWRevealViewController.h"

@interface IKTBucketsTableViewController () {
    UIView *titleView;
    BOOL isArtworksDataLoading;
    UITableViewCell *selectedCell;
    Bucket *selectedBucket;
}

@end

@implementation IKTBucketsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    selectedCell = [[UITableViewCell alloc] init];
    isArtworksDataLoading = NO;

    UIBarButtonItem *sidebarButton = [[Helper sharedInstance] createMainMenuButtonAtNavigationItemWithTarge:self.revealViewController AndAction:@selector(revealToggle:)];

    self.navigationItem.leftBarButtonItem = sidebarButton;
}

- (void) viewWillAppear:(BOOL)animated {
    LibraryAPI *library = [LibraryAPI sharedInstance];
    [library setDelegate:self];
    
    [super viewWillAppear:animated];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Buckets Table Screen"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)createBucketButtonTapped:(id)sender {
    [[Helper sharedInstance] showCreateBucketAlertViewWithDelegate:self];
}

#pragma mark - Table view data source

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([indexPath section] == 0) {
        [self performSegueWithIdentifier:@"showLikedArtworksForUser" sender:nil];
    } else {
        selectedBucket = [[self buckets] objectAtIndex:[indexPath row]];
        [[LibraryAPI sharedInstance] clearBucketedArtworks];
        [self performSegueWithIdentifier:@"showArtworksListForBucket" sender:selectedCell];
        
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath section] == 0) {
        static NSString *cellIdentifier = @"ArtworkCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:cellIdentifier];
        }
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell.textLabel setText:@"Show All"];
        return cell;
    } else {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = @"Liked Artwork";
            break;
        case 1:
            sectionName = @"Kollections";
            break;
            // ...
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return 2;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    IKTArtworksTableViewController *destinationController = [segue destinationViewController];
    [destinationController setSelectedBucket:selectedBucket];
}

@end
