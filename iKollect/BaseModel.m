//
//  BaseModel.m
//  iKollect
//
//  Created by mihata on 10/4/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

- (id) initWithId:(NSString *)uid AndName:(NSString *)name
{
    self = [super init];
    if (self) {
        _uid = uid;
        _name = name;
    }
    
    return self;
}

- (BOOL) isEqual:(id)object {
    if ([[object name] isEqual:_name])
        return YES;
    else
        return NO;
}
@end
