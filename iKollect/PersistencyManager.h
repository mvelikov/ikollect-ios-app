//
//  PersistencyManager.h
//  iKollect
//
//  Created by mihata on 5/19/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Artwork.h"
#import "Gallery.h"
#import "Tag.h"
#import "IKTCollection.h"

@interface PersistencyManager : NSObject
- (NSArray*) getArtworks;
- (Artwork*) popArtwork;
- (Artwork*) getDummyArtwork;
- (void) setArtworks:(NSArray*)artworksList;
- (void) addArtwork: (Artwork*)artwork;
- (void) clearArtworks;

- (NSArray*) getGalleries;
- (void) addGallery: (Gallery*)gallery;
- (void) clearGalleries;

- (NSArray*) getTags;
- (void) clearTags;
- (void) addTag: (Tag*) tag;

- (NSArray*) getBuckets;
- (void) setBuckets: (NSDictionary *) dataDictionary;
- (void) clearBuckets;
- (void) addBucket: (Bucket*) bucket;
- (int) totalBuckets;
- (int) currentPageBuckets;
- (int) perPageBuckets;
- (int) lastPageBuckets;

- (NSArray*) getBucketedArtworks;
- (void) clearBucketedArtworks;
- (void) addBucketedArtworks: (Artwork*) artwork;
- (void) setBucketedArtworks: (NSDictionary *) dataDictionary;
- (int) totalBucketedArtworks;
- (int) currentPageBucketedArtworks;
- (int) perPageBucketedArtworks;
- (int) lastPageBucketedArtworks;
@end
