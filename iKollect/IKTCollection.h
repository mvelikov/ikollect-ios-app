//
//  IKTCollection.h
//  IKOLLECT
//
//  Created by mihata on 10/7/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IKTCollection : NSObject
@property (nonatomic) int total, perPage, currentPage, lastPage, from, to;
@property (strong, nonatomic) NSMutableArray *data;

- (id) init;
- (id) initWithTotal: (int) total
             perPage: (int) perPage
         currentPage: (int) currentPage
            lastPage: (int) lastPage
                from: (int) from
                  to: (int) to
             andData: (NSMutableArray *) data;
- (void) addObject: (id) object;
- (NSUInteger) count;
- (id) objectAtIndex: (int) index;
- (void) removeObjectAtIndex: (int) index;
- (void) removeAllObjects;
@end
