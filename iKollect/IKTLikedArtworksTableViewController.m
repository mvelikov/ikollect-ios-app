//
//  IKTLikedArtworksTableViewController.m
//  IKOLLECT
//
//  Created by mihata on 10/9/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTLikedArtworksTableViewController.h"
#import "SWRevealViewController.h"

@implementation IKTLikedArtworksTableViewController

- (void) viewDidLoad {
    [[LibraryAPI sharedInstance] clearBucketedArtworks];
    [super viewDidLoad];
    
    [self setDetailsSegue:@"showArtworkDetailsFromLikedArtworksList"];    
    [self setTitle:@"Liked Artworks"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Liked Artworks Screen"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Base Paginator class

- (void) preloadItems {
    [[LibraryAPI sharedInstance] loadLikedArtworksForUser:[[User sharedInstance] uid]
                                                   atPage:[self currentPage]];
}

@end
