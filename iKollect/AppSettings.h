//
//  AppSettings.h
//  iKollect
//
//  Created by mihata on 9/26/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppSettings : NSObject
@property (strong, nonatomic) NSMutableArray *cityNames;
@property (strong, nonatomic) NSMutableDictionary *cityLocations, *artworkCategories, *defaultCategories;

+ (AppSettings*)sharedInstance;
- (id) initWithCities: (NSDictionary *) cities AndCategories: (NSDictionary *) categories;
@end
