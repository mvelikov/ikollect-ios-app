//
//  Artwork.m
//  iKollect
//
//  Created by mihata on 5/15/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "Artwork.h"

@implementation Artwork

@synthesize description;
-(NSString*) getDimensionsString
{
    NSNumberFormatter *formater = [[NSNumberFormatter alloc] init];
    [formater setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString* dimensionsString = [NSString stringWithFormat:@"%@ x %@ %@",
                                  [self.dimensions valueForKey: @"width"],
                                  [self.dimensions valueForKey: @"height"],
                                  [self.dimensions valueForKey: @"measure"]];
    return dimensionsString;
}

-(id) initWithTitle: (NSString*) title
             author: (NSString*) author
              genre: (NSString*) genre
            gallery: (Gallery*) gallery
        description: (NSString*) descriptionString
          technique: (NSString*) technique
             photos: (NSArray*) photos
               tags: (NSArray*) tags
           comments: (NSArray*) comments
              likes: (NSInteger) likes
              price: (NSString*) price
         dimensions: (NSDictionary*) dimensions
        geolocation: (CLLocation*) geolocation
            uid: (NSString *)uid
            andYear: (NSString *) year
{
    self = [super init];
    if (self) {
        self = [self initWithTitle: title
                            author: author
                       description: descriptionString
                            photos: photos
                           gallery: gallery
                         technique: technique
                               uid: uid
                     andDimensions: dimensions];
        _gallery = gallery;
        _genre = genre;
        _tags = tags;
        _comments = comments;
        _likes = likes;
        _price = price;
        _geolocation = geolocation;
        _year = year;
    }
    
    return self;
}

-(id) initWithTitle: (NSString*) title
             author: (NSString*)author
              genre: (NSString*)genre
            gallery: (Gallery*)gallery
        description: (NSString*)descriptionStr
          technique: (NSString*) technique
             photos: (NSArray*) photos
               tags: (NSArray*) tags
           comments: (NSArray*) comments
              likes: (NSInteger) likes
              price: (NSString*) price
         dimensions: (NSDictionary*) dimensions
        geolocation: (CLLocation*) geolocation
                uid: (NSString*) uid
            buckets: (NSArray *) buckets
            andYear: (NSString *) year {
    self = [super init];
    if (self) {
        self = [self initWithTitle:title
                            author:author
                             genre:genre
                           gallery:gallery
                       description:descriptionStr
                         technique:technique
                            photos:photos
                              tags:tags
                          comments:comments
                             likes:likes
                             price:price
                        dimensions:dimensions
                       geolocation:geolocation
                               uid:uid
                           andYear:year];
        Bucket *tmpBuck;
        _buckets = [[NSMutableArray alloc] init];
        for (id bck in buckets) {
            tmpBuck = [[Bucket alloc] initWithId:[bck valueForKey:@"id"]
                                      AndName:[bck valueForKey: @"name"]];
            [_buckets addObject:tmpBuck];
        }
    }
    
    return self;
}

-(id) initWithTitle:(NSString *)title
             author:(NSString *)author
        description:(NSString *)descr
             photos:(NSArray *)photos
            gallery:(Gallery *)gallery
          technique:(NSString *)technique
                uid:(NSString*) uid
      andDimensions:(NSDictionary *)dimensions
{
    self = [super init];
    if (self) {
        _uid = uid;
        _title = title;
        _author = author;
        _gallery = gallery;
        self.description = descr;
        _technique = technique;
        _photos = photos;
        _mainPhoto = [photos objectAtIndex:0];
        _dimensions = dimensions;
        
        _thumbnail = [self getThumbnailURLOutOfMainPhoto:_mainPhoto];
        _fillThumbnail = [self getThumbnailURLOutOfMainPhoto:_mainPhoto withType:@"fill"];
    }
    return self;
}

- (void) setPhotos:(NSArray *)photos
{
    if (_photos != photos) {
        _photos = photos;
        
        _mainPhoto = [photos objectAtIndex:0];
        _thumbnail = [self getThumbnailURLOutOfMainPhoto:_mainPhoto];
    }
}

- (NSString*) getThumbnailURLOutOfMainPhoto:(NSString*) photo
{
    return [self getThumbnailURLOutOfMainPhoto:photo withType:@"fit"];
}

- (NSString*) getThumbnailURLOutOfMainPhoto:(NSString*) photo withType: (NSString *)type
{
    return [self getThumbnailURLOutOfMainPhotoWithType:type width:640 andHeight:520];
}

- (NSString*) getThumbnailURLOutOfMainPhotoWithType: (NSString *)type width: (NSUInteger) width andHeight: (NSUInteger) height
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                  @"/v\\d+/" options:0 error:nil];
    NSString *template = [NSString stringWithFormat:@"/c_%@,w_%lu,h_%lu/", type, (unsigned long)width, (unsigned long)height];
    return [regex stringByReplacingMatchesInString:_mainPhoto
                                           options:0
                                             range:NSMakeRange(0, [_mainPhoto length])
                                      withTemplate:template];
}
@end
