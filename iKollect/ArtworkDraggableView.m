//
//  ArtWorkDraggableView.m
//  iKollect
//
//  Created by mihata on 5/14/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "ArtworkDraggableView.h"
#import "ArtworkOverlayView.h"



@interface ArtworkDraggableView ()

@property(nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property(nonatomic) CGPoint originalPoint;
@property(nonatomic, strong) ArtworkOverlayView *overlayView;

@end
@implementation ArtworkDraggableView
@synthesize title, image, author, dimensions, imageView, overlayView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragged:)];
        [self addGestureRecognizer:self.panGestureRecognizer];
        
        UIColor *textColor = [UIColor darkGrayColor];
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height - 55, frame.size.width, 30)];
        [title setTextAlignment:NSTextAlignmentCenter];
        [title setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
        [title setTextColor:textColor];

        [self addSubview:title];
        
        author = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height - 25, frame.size.width, 25)];
        [author setTextAlignment:NSTextAlignmentCenter];
        [author setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline]];
        [author setTextColor:textColor];

        [self addSubview:author];
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height - 55)];
        [imageView setContentMode: UIViewContentModeScaleAspectFit];
        [imageView setBackgroundColor:[UIColor clearColor]];
        [imageView setClipsToBounds:YES];

        [self addSubview:imageView];

        overlayView = [[ArtworkOverlayView alloc] initWithFrame:self.bounds];
        overlayView.alpha = 0;
        [self addSubview:overlayView];
        
        [self setBackgroundColor:[UIColor clearColor]];

    }
    return self;
}

- (void)dragged:(UIPanGestureRecognizer *)gestureRecognizer
{
    CGFloat xDistance = [gestureRecognizer translationInView:self].x * 0.5;
    CGFloat yDistance = [gestureRecognizer translationInView:self].y * 0.5;
    
    switch (gestureRecognizer.state) {
        case UIGestureRecognizerStateBegan:{
            self.originalPoint = self.center;
            break;
        };
        case UIGestureRecognizerStateChanged:{
            CGFloat rotationStrength = MIN(xDistance / 320, 1);
            CGFloat rotationAngel = (CGFloat) (2*M_PI/16 * rotationStrength);

            CGFloat scaleStrength = 1 - fabsf(rotationStrength) / 4;
            CGFloat scale = MAX(scaleStrength, 0.93);
            CGAffineTransform transform = CGAffineTransformMakeRotation(rotationAngel / -4);
            CGAffineTransform scaleTransform = CGAffineTransformScale(transform, scale, scale);
            self.transform = scaleTransform;
            self.center = CGPointMake(self.originalPoint.x + xDistance, self.originalPoint.y + yDistance);
            
            [self updateOverlay:xDistance];

            break;
        };
        case UIGestureRecognizerStateEnded: {
            CGFloat rotationStrength = MIN(xDistance / 320, 1);
            
            if (fabsf(rotationStrength) > 0.25) {

                id<ArtworkDraggableViewDelegate> strongDelegate = self.delegate;
                if ([strongDelegate respondsToSelector:@selector(artworkDraggableDidEndSwipingWithStrength:)]) {
                    [strongDelegate artworkDraggableDidEndSwipingWithStrength:rotationStrength];
                }

            }
            [self resetViewPositionAndTransformations];
            break;
        };
        case UIGestureRecognizerStatePossible:break;
        case UIGestureRecognizerStateCancelled:break;
        case UIGestureRecognizerStateFailed:break;
    }
}

- (void)updateOverlay:(CGFloat)distance
{
    if (distance > 0) {
        self.overlayView.mode = ArtworkViewModeRight;
    } else if (distance <= 0) {
        self.overlayView.mode = ArtworkViewModeLeft;
    }
    CGFloat overlayStrength = MIN(fabsf(distance) / 100, 0.4);

    self.overlayView.alpha = overlayStrength * 2;
}

- (void)resetViewPositionAndTransformations
{
    self.center = self.originalPoint;
    self.transform = CGAffineTransformMakeRotation(0);
    self.overlayView.alpha = 0;
}

- (void)dealloc
{
    [self removeGestureRecognizer:self.panGestureRecognizer];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
