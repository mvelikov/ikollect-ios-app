//
//  UserSettings.h
//  iKollect
//
//  Created by mihata on 9/5/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSettings : NSObject <NSCoding>
@property (nonatomic, strong) NSString *city, *address, *locationType, *searchFor;
@property (nonatomic) float latitude, longitude;
@property (nonatomic) int distance;
@property (nonatomic, strong) NSMutableDictionary *categories;
@end
