//
//  SettingsViewController.h
//  iKollect
//
//  Created by mihata on 7/2/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "IKTParentViewController.h"


@interface SettingsViewController : IKTParentViewController <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate, UIAlertViewDelegate, LibraryAPIDelegate, CLLocationManagerDelegate, SWRevealViewControllerDelegate, FBLoginViewDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIPickerView *cityPicker;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UIView *cityPickerViewContainer;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cityDoneButton;

@property (weak, nonatomic) IBOutlet UITextField *addressField;
@property (weak, nonatomic) IBOutlet UITextField *hiddenAddressField;
@property (weak, nonatomic) IBOutlet UISwitch *geoLocationSwitch;

@property (weak, nonatomic) IBOutlet UIView *cityWrapperView;
@property (weak, nonatomic) IBOutlet UIView *addressWrapperView;
@property (weak, nonatomic) IBOutlet UIView *geolocatoinWrapperView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *addressIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *geoLocationIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *successfullGeoLocationIndicator;

@property (weak, nonatomic) IBOutlet UIView *distanceWrapperView;
@property (weak, nonatomic) IBOutlet UILabel *distanceSliderLabel;
@property (weak, nonatomic) IBOutlet UISlider *distanceSlider;

@property (weak, nonatomic) IBOutlet UIView *accountWrapperView;
@property (weak, nonatomic) IBOutlet UIImageView *accountAvatar;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UIView *accountButton;
@property (weak, nonatomic) IBOutlet FBLoginView *facebookLoginView;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (weak, nonatomic) IBOutlet UISwitch *museumsSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *galleriesSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *artistsSwitch;

@property (weak, nonatomic) IBOutlet UIView *tagsLabesWrapperView;
@property (weak, nonatomic) IBOutlet UIView *tagsWrapperView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *tagsLoadingIndicator;

- (IBAction)doneButton:(id)sender;
- (IBAction)distanceSliderValueChanged:(id)sender;
- (IBAction)distanceSliderDragExit:(id)sender;
- (IBAction)sidebarButtonTapped:(id)sender;
- (IBAction)logoutButtonTapped:(id)sender;
- (IBAction)switchValueChanged:(UISwitch*)sender;


- (IBAction)searchForSwitchesValueChanged:(UISwitch *)sender;


@end
