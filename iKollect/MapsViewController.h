//
//  MapsViewController.h
//  iKollect
//
//  Created by mihata on 7/23/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "IKTParentViewController.h"

@interface MapsViewController : IKTParentViewController <MKMapViewDelegate, LibraryAPIDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
