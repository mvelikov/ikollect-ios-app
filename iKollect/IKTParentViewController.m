//
//  IKTParentViewController.m
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/13/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTParentViewController.h"
#import "SWRevealViewController.h"

@interface IKTParentViewController ()

@end

@implementation IKTParentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealController = [self revealViewController];
//    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
