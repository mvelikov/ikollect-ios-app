//
//  Helper.h
//  iKollect
//
//  Created by mihata on 9/23/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>


@interface Helper : NSObject <CLLocationManagerDelegate>
+ (Helper*)sharedInstance;

- (void) showIndicatorBehindImageView: (UIImageView *)imageView withImageURL: (NSURL*) imageUrl;
- (MFMailComposeViewController *) getMailComposerViewControllerWithRecipient: (NSString*) recipientEmail Subject: (NSString*) subject andBody: (NSString *) body;

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error;
- (UIBarButtonItem *) createMainMenuButtonAtNavigationItemWithTarge: (id) target AndAction: (SEL) action;

- (void) showCreateBucketAlertViewWithDelegate: (id) delegate;

- (void) startStandardLocationService;
//- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations;
//- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;
- (void) stopUpdatingLocation;
@end
