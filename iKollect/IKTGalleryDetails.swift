//
//  IKTGalleryDetails.swift
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/15/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

import UIKit

class IKTGalleryDetails: NSObject {

    var name: String = ""
    var country: String = ""
    var city: String = ""
    var address: String = ""
    var phone: String = ""
    var placeId: String = ""
    var openNow: Bool = false
    var email: String = ""
    
    var visitingHours: [IKTVisitingPeriod] = []
    
    override init() {
        super.init()
        name = ""
        country = ""
        city = ""
        address = ""
        phone = ""
        placeId = ""
        openNow = false
        email = ""
        visitingHours = []
    }
    init(name: String, country: String, city: String, address: String, phone: String, email: String, placeId: String, visitingHours: [IKTVisitingPeriod], openNow: Bool) {
        self.name = name
        self.country = country
        self.city = city
        self.address = address
        self.phone = phone
        self.placeId = placeId
        self.visitingHours = visitingHours
        self.openNow = openNow
        self.email = email
    }
}
