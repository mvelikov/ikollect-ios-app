//
//  IKTBucketsTableViewController.h
//  IKOLLECT
//
//  Created by mihata on 10/7/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKTBaseBucketsTableViewController.h"

@interface IKTBucketsTableViewController : IKTBaseBucketsTableViewController <UIAlertViewDelegate, LibraryAPIDelegate>

@end
