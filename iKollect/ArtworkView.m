//
//  ArtworkView.m
//  iKollect
//
//  Created by mihata on 5/14/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "ArtworkView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ArtworkView
@synthesize title, author, dimensions, gallery, draggableImage, draggableView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
//        [self loadDraggableCustomView];
        draggableView = [[ArtworkDraggableView alloc] initWithFrame:CGRectMake(0, frame.origin.y, frame.size.width, ARTWORK_VIEW_HEIGHT)];
    }
    return self;
}


- (void)loadDraggableCustomView
{
    
    [[Helper sharedInstance] showIndicatorBehindImageView:draggableView.imageView
                                             withImageURL:[NSURL URLWithString:draggableImage]];
    
    [draggableView.title setText:title];
    [draggableView.dimensions setText:dimensions];
    [draggableView.author setText:author];
    
    [self addSubview:draggableView];

}

@end
