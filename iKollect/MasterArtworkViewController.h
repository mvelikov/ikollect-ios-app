//
//  ViewController.h
//  iKollect
//
//  Created by mihata on 5/10/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArtworkView.h"
#import "ArtworkDraggableView.h"
#import "TransitionDelegate.h"
#import "IKTParentViewController.h"
#import "BucketPickerTableViewController.h"

@protocol ArtworkDraggableViewDelegate;

@interface MasterArtworkViewController : IKTParentViewController <ArtworkDraggableViewDelegate, LibraryAPIDelegate, BucketPickerDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) ArtworkView *artworkView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *detailsButton;

@property (weak, nonatomic) IBOutlet UIView *draggableViewWrapper;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingArtworkIndicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *dislikeVoteButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *likeVoteButton;
@property (weak, nonatomic) IBOutlet UIView *voteButtonsWrapperView;
@property (weak, nonatomic) IBOutlet UIButton *closeInstructionsButton;
@property (weak, nonatomic) IBOutlet UIImageView *instructionsImage;
@property (weak, nonatomic) IBOutlet UILabel *noMoreArtworksLabel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addToBucketButton;

@property (nonatomic, strong) TransitionDelegate *transitionController;

- (IBAction)likeButtonTapped:(id)sender;
- (IBAction)dislikeButtonTapped:(id)sender;
- (IBAction)closeInstructionsButtonTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addToBucketButtonTapped;


@end
