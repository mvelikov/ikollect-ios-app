//
//  RootViewController.m
//  iKollect
//
//  Created by mihata on 9/5/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];

    [self setRoundedCornersForViewElements];
    
    if ([[User sharedInstance] isLogged] == NO) {
        [self showModalView];
    }
    
    [self checkIfGeoLocationIsEnabledAndStartService];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Root Screen";
}

- (void) viewDidAppear:(BOOL)animated
{
    if ([[User sharedInstance] isLogged] == YES) {
        LibraryAPI *library = [LibraryAPI sharedInstance];
        library.delegate = self;
        [library loadAppSettings];
    }
    [super viewDidAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [[Helper sharedInstance] stopUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showModalView
{
    [self performSegueWithIdentifier:@"showModalLoginView" sender:self];
}

- (void) pushLoggedUserView
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    UIViewController *masterArtworkController = [storyboard instantiateViewControllerWithIdentifier:@"masterArtworkController"];
    [self.navigationController pushViewController:masterArtworkController animated:NO];
//    [self performSegueWithIdentifier:@"pushLoggedUserToMasterView" sender:self];
}

- (void) setRoundedCornersForViewElements
{
    float radius = 4.0f;
    [_initializeButton.layer setCornerRadius:radius];
    [_loadingIndicatorWrapper.layer setCornerRadius:radius];
}

- (IBAction)initializeButtonTapped:(id)sender {
    [[LibraryAPI sharedInstance] loadAppSettings];
    
    [_initializeButton setHidden:YES];
    [_loadingIndicatorWrapper setHidden:NO];
}

#pragma mark LibraryAPIDelegate

- (void) didLoadAppSettings:(NSDictionary *)settings
{
    (void) [[AppSettings sharedInstance] initWithCities:[settings valueForKey:@"cities"]
                                   AndCategories:[settings valueForKey:@"categories"]];
    [self pushLoggedUserView];
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [_initializeButton setHidden:NO];
    [_loadingIndicatorWrapper setHidden:YES];
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}

#pragma mark CLLocationManagerDelegate

- (void) checkIfGeoLocationIsEnabledAndStartService {
    
    UserSettings *userSettings = [[User sharedInstance] settings];
    
    if ([[userSettings city] isEqualToString:@"geoLocation"]) {
        [[Helper sharedInstance] startStandardLocationService];
    }
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [[Helper sharedInstance] locationManager:manager didUpdateLocations:locations];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    [[Helper sharedInstance] locationManager:manager didFailWithError:error];
}


@end
