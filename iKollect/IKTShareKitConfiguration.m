//
//  IKTShareKITConfiguration.m
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/13/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTShareKitConfiguration.h"

@implementation IKTShareKitConfiguration

- (NSString*) appName {
    return @"IKOLLECT - Discover Art Just for You, Nearby";
}
- (NSString*) appURL {
    return @"http://ikollect.net/";
}
- (NSString*)facebookAppId {
    return @"1517902445088630";
}

- (NSString*)facebookLocalAppId {
    return @"";
}

- (NSNumber*)forcePreIOS6FacebookPosting {
    return [NSNumber numberWithBool:true];
}

- (NSString*)twitterConsumerKey {
    return @"gKPhRi2lbNTcMipevjLJc4Sso";
}

- (NSString*)twitterSecret {
    return @"y0yBrWzeUxUecaWUnhyDnSAu22nEQRPE1qfyOhgc3RCpdn59Fd";
}

- (NSNumber*)twitterUseXAuth {
    return [NSNumber numberWithInt:0];
}

- (NSString*)twitterUsername {
    return @"ikollect";
}

- (NSString*) sharersPlistName {
    return @"IKTSharers.plist";
}

//- (NSNumber*)showActionSheetMoreButton {
//    return 0;
//}
@end
