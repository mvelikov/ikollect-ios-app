//
//  IKTBaseBucketsTableViewController.h
//  IKOLLECT
//
//  Created by mihata on 10/8/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bucket.h"
#import "IKTBasePaginatedTableViewController.h"

@interface IKTBaseBucketsTableViewController : IKTBasePaginatedTableViewController <UIAlertViewDelegate, LibraryAPIDelegate>
@property (strong, nonatomic) NSMutableArray *buckets;

- (UITableViewCell *)tableView:(UITableView *)tableView bucketCellForRowAtIndexPath:(NSIndexPath *)indexPath;
@end
