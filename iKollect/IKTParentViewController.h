//
//  IKTParentViewController.h
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/13/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface IKTParentViewController : GAITrackedViewController

@end
