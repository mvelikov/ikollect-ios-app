//
//  DetailsViewController.m
//  iKollect
//
//  Created by mihata on 5/17/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "DetailsViewController.h"
#import <ShareKit/ShareKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ArtworkAnnotation.h"
#import "UserAnnotation.h"
#import "JTSImageViewController.h"
#import "BucketPickerTableViewController.h"
#import "IKTGalleryDetailsViewController.h"

@interface DetailsViewController () {
}

@end

@implementation DetailsViewController
@synthesize detailsObject, scrollView, titleLabel, authorLabel, dimensionsLabel, mainImage, galleryLabel, descriptionsLabel, artworkMapView, imageButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (IBAction)shareButtonTapped:(id)sender {
    NSData *rawImage = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[detailsObject thumbnail]]];
    UIImage *image = [UIImage imageWithData:rawImage];
    
    NSString *detailTitle = [detailsObject title];
    NSString *title = [NSString stringWithFormat:@"Checkout the \"%@\" Artwork in #IKOLLECT - http://ikollect.net",
                       [detailTitle substringToIndex: MIN(60, [detailTitle length])]];
    SHKItem *item = [SHKItem image:image title:title];
    
    // Get the ShareKit action sheet
    SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
    
    // ShareKit detects top view controller (the one intended to present ShareKit UI) automatically,
    // but sometimes it may not find one. To be safe, set it explicitly
    [SHK setRootViewController:self];
    
    // Display the action sheet
//    [actionSheet showFromBarButtonItem:_shareButton animated:YES];
    [actionSheet showFromRect:self.view.frame inView:self.view animated:YES];
}

-(void) setDetailsObject:(Artwork *)object
{
    if (detailsObject != object) {
        detailsObject = object;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = [detailsObject title];

    LibraryAPI *library = [LibraryAPI sharedInstance];
    library.delegate = self;
    
    [self showDetailsMapAnnotations];

    titleLabel.text = [detailsObject title];
    [titleLabel setPreferredMaxLayoutWidth:titleLabel.frame.size.width];
    
    authorLabel.text = [detailsObject author];


    [[Helper sharedInstance] showIndicatorBehindImageView:mainImage
                                             withImageURL:[NSURL URLWithString:[detailsObject mainPhoto]]];
    
    [descriptionsLabel setText :[detailsObject description] ];

//    [descriptionsLabel setNumberOfLines:0];
    [descriptionsLabel setPreferredMaxLayoutWidth:descriptionsLabel.frame.size.width];
    
    [dimensionsLabel setText:[detailsObject getDimensionsString]];
    [galleryLabel setText:[[detailsObject gallery] name]];
    [_likesLabel setText:[NSString stringWithFormat:@"%ld", (long)[detailsObject likes]]];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"Details Artwork Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showDetailsMapAnnotations
{
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [detailsObject geolocation].coordinate.latitude;
    zoomLocation.longitude = [detailsObject geolocation].coordinate.longitude;
    
    NSMutableArray * annotationsToRemove = [ artworkMapView.annotations mutableCopy ] ;
    [ annotationsToRemove removeObject:artworkMapView.userLocation ];
    [ artworkMapView removeAnnotations:annotationsToRemove ];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, METERS_PER_MILE, METERS_PER_MILE);
    
    [artworkMapView setRegion:viewRegion animated:YES];
    [artworkMapView setShowsPointsOfInterest:NO];
    
    
    UserSettings *userSettings = [[User sharedInstance] settings];
    CLLocationCoordinate2D userLocation;
    userLocation.latitude = userSettings.latitude;
    userLocation.longitude = userSettings.longitude;
    
    UserAnnotation *userAnnotation = [UserAnnotation new];
    userAnnotation.coordinate = userLocation;
    userAnnotation.title = [[User sharedInstance] name];
    
    [artworkMapView addAnnotation:userAnnotation];
    
    ArtworkAnnotation *annotation = [ArtworkAnnotation new];
    annotation.coordinate = zoomLocation;
    annotation.title = [detailsObject title];
    
    [artworkMapView addAnnotation:annotation];
}
- (IBAction)imageButtonTapped:(id)sender {
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    
    [imageInfo setImage:[mainImage image]];

    imageInfo.referenceRect = imageButton.frame;
    imageInfo.referenceView = imageButton.superview;
    
    // Setup view controller
    JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                           initWithImageInfo:imageInfo
                                           mode:JTSImageViewControllerMode_Image
                                           backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
    
    // Present the view controller.
    [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if (sender == _addToBucketButton) {
        BucketPickerTableViewController *controller = [segue destinationViewController];
        [controller setDelegate:self];
        [controller setBucketedArtworks:[detailsObject buckets]];
    } else if (sender == _galleryDetailsButton) {
        IKTGalleryDetailsViewController *detailsController = [segue destinationViewController];
        detailsController.details = [[IKTGalleryDetails alloc] init];
        [detailsController.details setPlaceId:[[detailsObject gallery] placeId]];
    }
    
}


#pragma Implementing MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapInstance viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"ArtworkLocation";
    if ([annotation isKindOfClass:[UserAnnotation class]]) {
        identifier = @"UserLocation";
        MKAnnotationView *annotationView = (MKAnnotationView *) [artworkMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"user"];//here we use a nice image instead of the default pins
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    } else if ([annotation isKindOfClass:[ArtworkAnnotation class]]) {
        
        MKAnnotationView *annotationView = (MKAnnotationView *) [artworkMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"gallery"];
        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    }
    
    return nil;
}

#pragma mark LibraryAPI Delegate

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}

#pragma mark BucketPickerDelegate

- (void) userDidSelectBuckets: (NSArray*) selectedBuckets {
    NSLog(@"%@", selectedBuckets);
    if ([selectedBuckets count]) {
        [detailsObject setBuckets:[selectedBuckets mutableCopy]];
        [[LibraryAPI sharedInstance] addArtwork:[detailsObject uid] inBuckets:selectedBuckets];
    }
}

@end
