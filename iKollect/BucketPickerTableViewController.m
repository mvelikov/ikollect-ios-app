//
//  BucketPickerTableViewController.m
//  iKollect
//
//  Created by mihata on 10/5/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "BucketPickerTableViewController.h"

@interface BucketPickerTableViewController () {
    NSMutableDictionary* selectedCells;
}

@end

@implementation BucketPickerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedCells = [[NSMutableDictionary alloc] init];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    id <BucketPickerDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(userDidSelectBuckets:)]) {
        NSMutableArray *selectedBuckets = [[NSMutableArray alloc] init];
        for (id selected in selectedCells) {
            [selectedBuckets addObject:[[self buckets] objectAtIndex:[selected intValue]]];
        }
        [strongDelegate userDidSelectBuckets:selectedBuckets];
    }
    
    [super viewWillDisappear:animated];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Bucket Picker Screen"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source



- (UITableViewCell *)tableView:(UITableView *)tableView bucketCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [super tableView:tableView bucketCellForRowAtIndexPath:indexPath];
    
    long row = [indexPath row];
    NSString* isCellSelected = [selectedCells objectForKey:[NSString stringWithFormat:@"%ld", row]];
    // need to check only if the cell is no already selected
    if (!isCellSelected) {
        Bucket *bucket = [[self buckets] objectAtIndex:row];
        if ([_bucketedArtworks containsObject:bucket]) {
            isCellSelected = @"YES";
            [selectedCells setObject:cell.textLabel.text
                              forKey:[NSString stringWithFormat:@"%ld", row]];
        }
    }

    if (isCellSelected != nil) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }

    return cell;
}


-(void)checkOrUncheckTableCell: (UITableViewCell*)thisCell withIndex:(NSIndexPath*)indexPath {
    if (thisCell.accessoryType == UITableViewCellAccessoryCheckmark) {
        thisCell.accessoryType = UITableViewCellAccessoryNone;
        [selectedCells removeObjectForKey:[NSString stringWithFormat:@"%ld",  (long)indexPath.row]];
    } else {
        thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
        [selectedCells setObject:thisCell.textLabel.text
                          forKey:[NSString stringWithFormat:@"%ld",  (long)indexPath.row]];
    }
}

-(void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
    [self checkOrUncheckTableCell:thisCell withIndex:indexPath];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
    
    [self checkOrUncheckTableCell:thisCell withIndex:indexPath];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
