//
//  BucketPickerTableViewController.h
//  iKollect
//
//  Created by mihata on 10/5/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKTBaseBucketsTableViewController.h"

@protocol BucketPickerDelegate;

@interface BucketPickerTableViewController : IKTBaseBucketsTableViewController <UIAlertViewDelegate, LibraryAPIDelegate>
@property (nonatomic, weak) id <BucketPickerDelegate> delegate;
@property (strong, nonatomic) NSArray *bucketedArtworks;

@end

@protocol BucketPickerDelegate <NSObject>

- (void) userDidSelectBuckets: (NSArray*) selectedBuckets;

@end
