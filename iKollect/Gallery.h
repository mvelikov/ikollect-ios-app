//
//  Gallery.h
//  iKollect
//
//  Created by mihata on 7/24/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Gallery : NSObject
@property (nonatomic, strong) NSString *uid, *name, *country, *city, *address, *postcode, *placeId, *phone, *rating;
@property (nonatomic, strong) CLLocation *location;

- (id) initWithId: (NSString*) uid
             name: (NSString*) name
          country: (NSString*) country
             city: (NSString*) city
          address: (NSString*) address
         postcode: (NSString*) postcode
          placeId: (NSString*) placeId
            phone: (NSString*) phone
           rating: (NSString*) rating
      andLocation: (CLLocation*) location;
@end
