//
//  Helper.m
//  iKollect
//
//  Created by mihata on 9/23/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "Helper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SWRevealViewController.h"

@interface Helper () {
    CLLocationManager *locationManager;
    BOOL isAlertViewShown;
}

@end
@implementation Helper

- (id) init {
    if (self = [super init]) {
        isAlertViewShown = NO;
    }
    return self;
}

+ (Helper*) sharedInstance
{
    // 1
    static Helper *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[Helper alloc] init];
    });
    return _sharedInstance;
}

- (void) showIndicatorBehindImageView: (UIImageView *)imageView withImageURL: (NSURL*) imageUrl
{
    for (UIView *view in [imageView subviews]) {
        [view removeFromSuperview];
    }
    __block UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [activityIndicator setCenter:[imageView center]];
    [activityIndicator setHidesWhenStopped:YES];
    [activityIndicator setHidden:NO];
    
    [activityIndicator startAnimating];
    
    [imageView addSubview:activityIndicator];
    [imageView sd_setImageWithURL:imageUrl
                 placeholderImage:[UIImage imageNamed:@""]
                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            [activityIndicator removeFromSuperview];
                        }];
}

- (MFMailComposeViewController *) getMailComposerViewControllerWithRecipient: (NSString*) recipientEmail Subject: (NSString*) subject andBody: (NSString *) body
{
    MFMailComposeViewController *mailer;
    
    if ([MFMailComposeViewController canSendMail])
    {
        mailer = [[MFMailComposeViewController alloc] init];
        
        [mailer setSubject:subject];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:recipientEmail, nil];
        [mailer setToRecipients:toRecipients];

        [mailer setMessageBody:body isHTML:NO];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    return mailer;
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:[error localizedDescription]
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (UIBarButtonItem *) createMainMenuButtonAtNavigationItemWithTarge: (id) target AndAction: (SEL) action {
    UIBarButtonItem *sidebarButton = [[UIBarButtonItem alloc]
                                      initWithImage:[UIImage imageNamed:@"menu.png"]
                                      style:UIBarButtonItemStyleBordered
                                      target:target
                                      action:action];
    return sidebarButton;
}

- (void) showCreateBucketAlertViewWithDelegate: (id) delegate
{
    UIAlertView *alertView = [[UIAlertView alloc ] initWithTitle:@"Kollection Name"
                                                         message:@"Enter the name of the kollection."
                                                        delegate:nil
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"OK", nil];
    
    alertView.delegate = delegate;
    
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [[alertView textFieldAtIndex:0] setAutocapitalizationType:UITextAutocapitalizationTypeSentences];
    [[alertView textFieldAtIndex:0] setText: @""];
    
    
    [alertView show];
}

- (void) startStandardLocationService {
    
    if (nil == locationManager) {
        locationManager = [[CLLocationManager alloc] init];
    }
    
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationManager requestWhenInUseAuthorization];
    }

    [self setLocationManagerSettings];
}

- (void) setLocationManagerSettings {
    if (nil == locationManager) {
        locationManager = [[CLLocationManager alloc] init];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    locationManager.distanceFilter = 100;
    
    [locationManager startUpdatingLocation];
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject];
    
    UserSettings *userSettings = [[User sharedInstance] settings];
    [userSettings setLatitude:currentLocation.coordinate.latitude];
    [userSettings setLongitude:currentLocation.coordinate.longitude];
    
    [userSettings setLocationType:@"getLocation"];
    [userSettings setCity:@"geoLocation"];
    
    [[User sharedInstance] saveUserSettings];
}

- (void) locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [locationManager stopUpdatingLocation];
    NSString *title, *message;
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied) {
        // The user denied your app access to location information.
        title = @"Please enable location services for iKollect App";
        message = @"Go to Settungs > Privacy > Location Services and switch iKollect to ON";
    } else {
        title = @"We are sorry, there was location issue";
        message = @"Please, check Your Preferences";
        
        UserSettings *defSettings = [[UserSettings alloc] init],
        *userSettings = [[User sharedInstance] settings];
        [userSettings setCity:[defSettings city]];
        [userSettings setLatitude:[defSettings latitude]];
        [userSettings setLongitude:[defSettings longitude]];
        [[User sharedInstance] saveUserSettings];
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    if (isAlertViewShown == NO) {
        [alert show];
        isAlertViewShown = YES;
    }
    
}

- (void) locationManager:(CLLocationManager *) manager
didChangeAuthorizationStatus:(CLAuthorizationStatus) status {

    [self setLocationManagerSettings];
}

- (void) stopUpdatingLocation {
    if (nil != locationManager) {
        [locationManager stopUpdatingLocation];
    }
}

@end
