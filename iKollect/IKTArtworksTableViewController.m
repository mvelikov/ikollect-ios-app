//
//  IKTArtworkTableViewController.m
//  IKOLLECT
//
//  Created by mihata on 10/7/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTArtworksTableViewController.h"
#import "IKTArtworkTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailsViewController.h"

@interface IKTArtworksTableViewController () {
    BOOL isArtworkDataLoading;
    UITableViewCell *selectedCell;
    Artwork *artworkObj;
}

@end

@implementation IKTArtworksTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isArtworkDataLoading = NO;
    
    [self setDetailsSegue:@"showArtworkDetailsFromArtworkList"];

    [self setTitle:[_selectedBucket name]];
    LibraryAPI *library = [LibraryAPI sharedInstance];
    [library setDelegate:self];
    
    _artworks = [[NSMutableArray alloc] initWithArray:[[library getBucketedArtworks] mutableCopy]];
    [self setItems:_artworks];
    [self setCurrentPage:[library currentPageBucketedArtworks]];
    [self setPages:[library lastPageBucketedArtworks]];
    [self setTotalItems:[library totalBucketedArtworks]];
}

- (void) viewWillAppear:(BOOL)animated {
    LibraryAPI *library = [LibraryAPI sharedInstance];
    [library setDelegate:self];
    
    [super viewWillAppear:animated];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Artwork Table Screen"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([indexPath row] < [_artworks count]) {
        return [self tableView:tableView artworkCellForRowAtIndexPath:indexPath];
    } else {
        return [super tableView:tableView loadingCellForRowAtIndexPath:indexPath];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView artworkCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ArtworkCell";
    IKTArtworkTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[IKTArtworkTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
    }

    Artwork *artwork = [_artworks objectAtIndex:[indexPath row]];
    NSURL *thumbnailUrl = [NSURL URLWithString:[artwork
                                                getThumbnailURLOutOfMainPhotoWithType:@"fill"
                                                                                width:264
                                                                            andHeight:174]];

    [cell.picture sd_setImageWithURL:thumbnailUrl
                      placeholderImage:[UIImage imageNamed:IMAGE_PLACEHOLDER]];
    
    cell.title.text = [artwork title];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    if (isArtworkDataLoading == NO) {
        isArtworkDataLoading = YES;
        
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activityIndicator startAnimating];
        [activityIndicator hidesWhenStopped];
        
        UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
        [thisCell setAccessoryView:activityIndicator];
        selectedCell = thisCell;
        
        Artwork *artwork = [_artworks objectAtIndex:[indexPath row]];
        [[LibraryAPI sharedInstance] loadArtworkWithId:[artwork uid]];
    }
    
}

#pragma mark - Base Paginator class

- (void) preloadItems {
    [[LibraryAPI sharedInstance] loadArtworksFromBucket:[_selectedBucket uid] atPage: [self currentPage]];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if (artworkObj != nil) {
        DetailsViewController *destinationController = [segue destinationViewController];
        [destinationController setDetailsObject:artworkObj];
    }
}


#pragma mark Library API Delegate

- (void) libraryDidLoadArtwork:(Artwork *) artwork {
    if (artwork != nil) {
        if (selectedCell != nil) {
            [selectedCell setAccessoryView:nil];
            isArtworkDataLoading = NO;
        }
        artworkObj = artwork;
        [self performSegueWithIdentifier:[self detailsSegue] sender:selectedCell];
    }
}

- (void) showErrorViewWithTitle:(NSString *)title andError:(NSError *)error {
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}

- (void) libraryDidLoadBucketedArtworks {
    LibraryAPI *library = [LibraryAPI sharedInstance];
    
    _artworks = [[library getBucketedArtworks] mutableCopy];
    
    [self setItems:_artworks];
    [self setCurrentPage:[library currentPageBucketedArtworks]];
    [self setPages:[library lastPageBucketedArtworks]];
    [self setTotalItems:[library totalBucketedArtworks]];
    
    [self.tableView reloadData];
}
@end
