//
//  IKTArtworkTableViewController.h
//  IKOLLECT
//
//  Created by mihata on 10/7/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Artwork.h"
#import "Bucket.h"
#import "IKTBasePaginatedTableViewController.h"

@interface IKTArtworksTableViewController : IKTBasePaginatedTableViewController <LibraryAPIDelegate>

@property (strong, nonatomic) NSString *detailsSegue;
@property (strong, nonatomic) NSMutableArray *artworks;
@property (strong, nonatomic) Bucket *selectedBucket;
@end
