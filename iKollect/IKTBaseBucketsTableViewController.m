//
//  IKTBaseBucketsTableViewController.m
//  IKOLLECT
//
//  Created by mihata on 10/8/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "IKTBaseBucketsTableViewController.h"
#import "SWRevealViewController.h"

@implementation IKTBaseBucketsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    SWRevealViewController *revealController = [self revealViewController];
//    [revealController panGestureRecognizer];
    [revealController tapGestureRecognizer];
    
    LibraryAPI *library = [LibraryAPI sharedInstance];
    [library setDelegate:self];
    [self setTitle:@"Kollections"];
    
    _buckets = [[NSMutableArray alloc] initWithArray:[[library getBuckets] mutableCopy]];

    [self setItems:_buckets];
    [self setCurrentPage:[library currentPageBuckets]];
    [self setPages:[library lastPageBuckets]];
    [self setTotalItems:[library totalBuckets]];
    
    UIBarButtonItem *createBucketButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createBucketButtonTapped:)];
    
    self.navigationItem.rightBarButtonItem = createBucketButton;
}

- (void) viewWillAppear:(BOOL)animated {
    LibraryAPI *library = [LibraryAPI sharedInstance];
    [library setDelegate:self];
    
    [super viewWillAppear:animated];
    
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Bucket Picker Screen"];
    
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (IBAction)createBucketButtonTapped:(id)sender {
    [[Helper sharedInstance] showCreateBucketAlertViewWithDelegate:self];
}

#pragma mark - Base Paginator class

- (void) preloadItems {
    NSString *uid = [[User sharedInstance] uid];
    [[LibraryAPI sharedInstance] loadBucketsListForUser:uid atPage:[self currentPage]];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView bucketCellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellIdentifier];
    }
    
    // Configure the cell...
    Bucket *bucket = [_buckets objectAtIndex:[indexPath row]];
    [[cell textLabel] setText:[bucket name]];
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([indexPath row] < [_buckets count]) {
        return [self tableView:tableView bucketCellForRowAtIndexPath:indexPath];
    } else {
        return [super tableView:tableView loadingCellForRowAtIndexPath:indexPath];
    }
}

#pragma mark - Alert View Delegate Methods

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    UITextField *bucketTextField = [alertView textFieldAtIndex:0];
    
    if (buttonIndex == 1 && ![bucketTextField.text isEqualToString:@""]) { // OK button tapped
        
        [[LibraryAPI sharedInstance] createBucketWithName:[bucketTextField text]
                                                  forUser:[[User sharedInstance] uid]];
        
        UIActivityIndicatorView *aiView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        aiView.hidesWhenStopped = NO; //I added this just so I could see it
        [aiView startAnimating];

        self.navigationItem.titleView = aiView;
    }
}

#pragma mark LibraryAPI Delegate
- (void) libraryDidLoadBuckets
{
    self.navigationItem.titleView = nil;
    LibraryAPI *library = [LibraryAPI sharedInstance];
    _buckets = [[library getBuckets] mutableCopy];
    
    [self setItems:_buckets];
    [self setCurrentPage:[library currentPageBuckets]];
    [self setPages:[library lastPageBuckets]];
    [self setTotalItems:[library totalBuckets]];

    [self.tableView reloadData];
}

- (void) showErrorViewWithTitle:(NSString *)title andError:(NSError *)error {
    [[Helper sharedInstance] showErrorViewWithTitle:title andError:error];
}


@end
