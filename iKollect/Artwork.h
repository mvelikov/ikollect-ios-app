//
//  Artwork.h
//  iKollect
//
//  Created by mihata on 5/15/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Bucket.h"
#import "Gallery.h"

@interface Artwork : NSObject
// titile, author, jender, photos, categories likes, price, description, tags, comments, technique, dimensions, geolocation, year
@property (nonatomic, strong) NSString *title, *author, *genre, *description, *technique, *uid, *year;
@property (nonatomic, strong, readonly) NSString *mainPhoto, *thumbnail, *fillThumbnail;
@property (nonatomic, strong) NSArray *photos, *tags, *comments;
@property (nonatomic) NSInteger likes;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSDictionary *dimensions;
@property (nonatomic, strong) CLLocation *geolocation;
@property (nonatomic, strong) Gallery *gallery;

@property (nonatomic) NSMutableArray *buckets;

-(NSString*) getDimensionsString;
-(id) initWithTitle: (NSString*) title
             author: (NSString*)author
              genre: (NSString*)genre
            gallery: (Gallery*)gallery
        description: (NSString*)description
          technique: (NSString*) technique
             photos: (NSArray*) photos
               tags: (NSArray*) tags
           comments: (NSArray*) comments
              likes: (NSInteger) likes
              price: (NSString*) price
         dimensions: (NSDictionary*) dimensions
        geolocation: (CLLocation*) geolocation
            uid: (NSString*) uid
            andYear: (NSString *) year;

-(id) initWithTitle: (NSString*) title
             author: (NSString*)author
              genre: (NSString*)genre
            gallery: (Gallery*)gallery
        description: (NSString*)description
          technique: (NSString*) technique
             photos: (NSArray*) photos
               tags: (NSArray*) tags
           comments: (NSArray*) comments
              likes: (NSInteger) likes
              price: (NSString*) price
         dimensions: (NSDictionary*) dimensions
        geolocation: (CLLocation*) geolocation
                uid: (NSString*) uid
            buckets: (NSArray *) buckets
            andYear: (NSString *) year;


-(id) initWithTitle: (NSString*) title
             author: (NSString*) author
        description: (NSString*) description
             photos: (NSArray*) photos
            gallery: (Gallery*) gallery
          technique: (NSString*) technique
            uid: (NSString*) uid
      andDimensions: (NSDictionary*) dimensions;

- (void) setPhotos:(NSArray *)photos;
- (NSString*) getThumbnailURLOutOfMainPhotoWithType: (NSString *)type width: (NSUInteger) width andHeight: (NSUInteger) height;
@end
