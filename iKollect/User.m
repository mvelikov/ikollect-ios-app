//
//  User.m
//  iKollect
//
//  Created by mihata on 5/25/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "User.h"

@implementation User

- (id) init {
    if (self = [super init]) {
        [self clearUser];
    }
    return self;
}

+ (User*) sharedInstance
{
    // 1
    static User *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[User alloc] init];
    });
    return _sharedInstance;
}

- (void) setUserUid: (NSString*) uid Name: (NSString*) name Username: (NSString*) username Email: (NSString*) email Avatar: (NSURL*) avatar AndSex: (NSString*) sex
{
    _uid = uid;
    _username = username;
    _name = name;
    _email = email;
    _avatar = avatar;
    _sex = sex;
    _isLogged = YES;
}

- (void) clearUser
{
    _uid = @"";
    _name = @"";
    _username = @"";
    _avatar = [[NSURL alloc] initWithString:@""];
    _sex = @"";
    _settings = [[UserSettings alloc] init];
    _isLogged = NO;
    _loginType = @"";
}

- (BOOL) loadUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *userEncodedObject = [defaults objectForKey:@"defaultUser"];
    
    NSDictionary *defaultUser = [NSKeyedUnarchiver unarchiveObjectWithData:userEncodedObject];


    if (defaultUser != nil) {
        _uid = [NSString stringWithFormat:@"%@",[defaultUser objectForKey:@"uid"]];
        _username = [defaultUser objectForKey:@"username"];
        _name = [defaultUser objectForKey:@"name"];
        _email = [defaultUser objectForKey:@"email"];
        _avatar = [defaultUser objectForKey:@"avatar"];
        _sex = [defaultUser objectForKey:@"sex"];
        _loginType = [defaultUser objectForKey:@"login"];
        
        if ([_uid isEqualToString:@""]) {
            _isLogged = NO;
        } else {
            _isLogged = YES;
        }
        
        [self loadUserSettings];
        return YES;
    } else {
        return NO;
    }
}

- (void) saveUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *userDictionary = [[NSMutableDictionary alloc] init];
    
    if (_name != nil)
        [userDictionary setObject:_name forKey:@"name"];
    
    if (_username != nil)
        [userDictionary setObject:_username forKey:@"username"];
    
    if (_uid != nil)
        [userDictionary setObject:_uid forKey:@"uid"];
    
    if (_email != nil)
        [userDictionary setObject:_email forKey:@"email"];
    
    if (_avatar != nil)
        [userDictionary setObject:_avatar forKey:@"avatar"];

    if (_sex != nil)
        [userDictionary setObject:_sex forKey:@"sex"];
    
    if (_loginType != nil)
        [userDictionary setObject:_loginType forKey:@"login"];
    
    [self saveUserSettings];

    NSData *userEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:userDictionary];

    [defaults setObject:userEncodedObject forKey:@"defaultUser"];
    [defaults synchronize];
}

- (void) saveUserSettings
{
    NSData *settingsEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:_settings];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:settingsEncodedObject forKey:@"defaultUserSettings"];
    
    [defaults synchronize];
}

- (void) loadUserSettings
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *settingsEncodedObject = [defaults objectForKey:@"defaultUserSettings"];
    
    _settings = [NSKeyedUnarchiver unarchiveObjectWithData:settingsEncodedObject];
}

@end
