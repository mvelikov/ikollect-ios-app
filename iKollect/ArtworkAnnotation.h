//
//  ArtworkLocation.h
//  iKollect
//
//  Created by mihata on 9/1/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface ArtworkAnnotation : MKPointAnnotation

@end
