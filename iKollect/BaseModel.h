//
//  BaseModel.h
//  iKollect
//
//  Created by mihata on 10/4/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject
@property (nonatomic, strong) NSString *uid, *name;

- (id) initWithId: (NSString*) uid
          AndName: (NSString*) name;
@end
