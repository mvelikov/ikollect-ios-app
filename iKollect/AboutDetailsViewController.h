//
//  AboutDetailsViewController.h
//  iKollect
//
//  Created by mihata on 9/10/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface AboutDetailsViewController : GAITrackedViewController
@property (strong, nonatomic) NSString *aboutUrl;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (weak, nonatomic) IBOutlet UILabel *aboutTextLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

- (IBAction)doneButtonTapped:(id)sender;
@end
