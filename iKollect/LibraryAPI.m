//
//  LibraryAPI.m
//  iKollect
//
//  Created by mihata on 5/19/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "LibraryAPI.h"
#import "PersistencyManager.h"
#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import "IKOLLECT-Swift.h"


@interface LibraryAPI () {
    PersistencyManager *persistencyManager;
    HTTPClient *httpClient;
}

@end

@implementation LibraryAPI


- (id) init
{
    self = [super init];
    
    if (self) {
        persistencyManager = [[PersistencyManager alloc] init];
        httpClient = [[HTTPClient alloc] init];
        httpClient.delegate = self;
    }
    return self;
}

+ (LibraryAPI*) sharedInstance
{
    // 1
    static LibraryAPI *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[LibraryAPI alloc] init];
    });
    return _sharedInstance;
}

- (NSArray*) getArtworks
{
    return [persistencyManager getArtworks];
}

- (Artwork*) getArtwork
{
    return [persistencyManager popArtwork];
}

- (Artwork*) getDummyArtwork
{
    return [persistencyManager getDummyArtwork];
}

- (void) loadArtworks
{
//    [httpClient getRequest:@"/getter.php?collection=artworks"];
    [self loadArtworksPage:0];
}

- (void) loadTagsPage:(int) page
{
    int uid = [[[User sharedInstance] uid] intValue];
    [httpClient getTagsForUser: uid AtPage: page];
}

- (void) loadArtworksPage:(int) page
{
    UserSettings *userSettings = [[User sharedInstance] settings];
    float latitude = [userSettings latitude];
    float longitude = [userSettings longitude];
    int distance = [userSettings distance];
    NSDictionary *categories = [userSettings categories];
    NSString *categoriesString = @"0";
    
    for (id category in categories) {
        categoriesString = [categoriesString stringByAppendingString:[NSString stringWithFormat:@"%@,", category]];
    }
    
    [httpClient getArtworksForUser:[[User sharedInstance] uid] atLatitude:latitude AndLongitude:longitude InDistance:distance ForCategories:categoriesString AtPage:page];
}

- (void) loadGalleries
{
    
    float latitude = [[[User sharedInstance] settings] latitude];
    float longitude = [[[User sharedInstance] settings] longitude];
    int distance = [[[User sharedInstance] settings] distance];
    
    [httpClient getGalleriesNearLatitude: latitude
                            AndLongitude:longitude
                             AndDistance: distance
                                 forUser:[[User sharedInstance] uid]];
}

- (void) addGallery: (Gallery*) gallery
{
    [persistencyManager addGallery:gallery];
}
- (NSArray*) getGalleries
{
    return [persistencyManager getGalleries];
}

- (void) voteForArtwork: (Artwork*) artwork withVote : (NSInteger) vote
{
    User *user = [User sharedInstance];
    [httpClient sendVoteRequestForUser:[user uid]
                            andArtwork:[artwork uid]
                              withVote:vote
                            atLatitude:[[user settings] latitude]
                          andLongitude:[[user settings] longitude]];
}

- (void) getGeoLocationForAddress: (NSString*)address
{
    [httpClient getGeoLocationForAddress:[address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

- (void) loginUserWithEmail: (NSString*) email andPassword: (NSString*) password
{
    [httpClient loginUserWithEmail: email andPassword: password];
}

- (void) createCustomerAccountForFacebookUser:(id)user
{
    [httpClient createCustomerProfileForFacebookUser:(id)user];
}

- (NSArray*) getTags
{
    return [persistencyManager getTags];
}
- (void) clearTags
{
    [persistencyManager clearTags];
}
- (void) addTag: (Tag*) tag
{
    [persistencyManager addTag:tag];
}

- (NSArray*) getBuckets
{
    return [persistencyManager getBuckets];
}
- (void) clearBuckets
{
    [persistencyManager clearBuckets];
}
- (void) addBucket: (Bucket*) bucket
{
    [persistencyManager addBucket:bucket];
}
- (int) totalBuckets {
    return [persistencyManager totalBuckets];
}
- (int) currentPageBuckets {
    return [persistencyManager currentPageBuckets];
}
- (int) perPageBuckets {
    return [persistencyManager perPageBuckets];
}
- (int) lastPageBuckets {
    return [persistencyManager lastPageBuckets];
}


- (NSArray*) getBucketedArtworks {
    return [persistencyManager getBucketedArtworks];
}
- (void) clearBucketedArtworks {
    [persistencyManager clearBucketedArtworks];
}
- (void) addBucketedArtworks: (Artwork*) artwork {
    [persistencyManager addBucketedArtworks:artwork];
}


- (int) totalBucketedArtworks {
    return [persistencyManager totalBucketedArtworks];
}
- (int) currentPageBucketedArtworks {
    return [persistencyManager currentPageBucketedArtworks];
}
- (int) perPageBucketedArtworks {
    return [persistencyManager perPageBucketedArtworks];
}
- (int) lastPageBucketedArtworks {
    return [persistencyManager lastPageBucketedArtworks];
}


- (void) loadAppSettings
{
    [httpClient loadAppSettings];
}

- (void) loadBucketsListForUser: (NSString *) user
{
    [httpClient loadBucketsListForUser:user atPage:1];
}

- (void) loadBucketsListForUser: (NSString *) user atPage: (int) page {
    [httpClient loadBucketsListForUser:user atPage:page];
}

- (void) createBucketWithName:(NSString*) name forUser: (NSString *) user
{
    [httpClient createBucketWithName:name forUser:user];
}

- (void) addArtwork:(NSString *) artwork inBuckets: (NSArray *)buckets {

    NSString *bucketsString = @"";
    for (id buck in buckets) {
        NSString *uid = [buck uid];

        bucketsString = [bucketsString stringByAppendingFormat:@"%@,", uid];
    }

    [httpClient addArtwork:artwork toBuckets:bucketsString];
}

- (void) loadArtworksFromBucket: (NSString *)bucket atPage: (NSUInteger) page {
    [httpClient loadArtworksFromBucket:bucket atPage:page];
}
- (void) loadArtworksFromBucket: (NSString *)bucket
{
    [httpClient loadArtworksFromBucket:bucket atPage:0];
}

- (void) loadLikedArtworksForUser: (NSString *) user atPage: (NSUInteger) page {
    [httpClient loadLikedArtworksForUser:user atPage:page];
}

- (void) loadArtworkWithId: (NSString *)uid {
    User *user = [User sharedInstance];
    [httpClient loadArtworkWithId:uid forUser:[user uid] atLatitude:[[user settings] latitude] andLongitude:[[user settings] longitude]];
}

- (void) loadGalleryDetailsWithPlaceId:(NSString*) placeId {
    [httpClient loadGalleryDetailsWithPlaceId:placeId];
}

- (NSArray *) parseArtworksResponse: (id) responseObject {
    Artwork *artwork;
    Gallery *gallery;
    CLLocation *location;
    NSMutableArray *artworksArray = [[NSMutableArray alloc] init];
    for (id itr in [responseObject valueForKey:@"data"]) {
        
        NSDictionary *tmpDict = itr;
        NSArray *likes;
        int likesCnt = 0;

        NSMutableArray* photos = [[NSMutableArray alloc] init];
        for (id photo in [tmpDict valueForKey:@"pictures"]) {
            [photos addObject:[photo valueForKey:@"url"]];
        }
        
        float latitude, longitude;
        
        latitude = [[[[tmpDict valueForKey:@"gallery" ] valueForKey:@"location" ] valueForKey:@"lat" ] floatValue];
        longitude = [[[[tmpDict valueForKey:@"gallery" ] valueForKey:@"location" ] valueForKey:@"lng" ] floatValue];
        likes = [[NSArray alloc] initWithArray:[tmpDict valueForKey:@"likes_count_relation"]];
        @try {
            likesCnt = [[[likes objectAtIndex:0] valueForKey:@"count"] intValue];
        }
        @catch (NSException *exception) {
            likesCnt = 0;
        }
        location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        
        NSDictionary *tmpGallery = [tmpDict valueForKey:@"gallery"];
        gallery = [[Gallery alloc] initWithId:[tmpGallery valueForKey:@"id"]
                                         name:[tmpGallery valueForKey:@"name"]
                                      country:[tmpGallery valueForKey:@"country"]
                                         city:[tmpGallery valueForKey:@"locality"]
                                      address:[tmpGallery valueForKey:@"formatted_address"]
                                     postcode:[tmpGallery valueForKey:@"postal_code"]
                                      placeId:[tmpGallery valueForKey:@"place_id"]
                                        phone:@""
                                       rating:@""
                                  andLocation:location];
        

        artwork = [[Artwork alloc] initWithTitle:[tmpDict valueForKey:@"title"]
                                          author:[[tmpDict valueForKey:@"author"] valueForKey:@"name" ]
                                           genre:[tmpDict valueForKey:@"genre"]
                                         gallery:gallery
                                     description:[tmpDict valueForKey:@"description"]
                                       technique:[tmpDict valueForKey:@"technique"]
                                          photos:photos
                                            tags:[tmpDict valueForKey:@"tags"]
                                        comments:[tmpDict valueForKey:@"comments"]
                                           likes:likesCnt
                                           price:[tmpDict valueForKey:@"price"]
                                      dimensions:[tmpDict valueForKey:@"dimensions"]
                                     geolocation:location
                                             uid:[tmpDict valueForKey:@"id"]
                                         buckets:[tmpDict valueForKey:@"buckets"]
                                         andYear:[tmpDict valueForKey:@"year"]];
        
//        [persistencyManager addArtwork:artwork];
        [artworksArray addObject:artwork];
    }
    return artworksArray;
}

#pragma mark - HTTPClientDelegate

- (void) loadDataFromServerWithResponse:(id)responseObject
{
    [persistencyManager setArtworks:[self parseArtworksResponse:responseObject]];
    
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(libraryDidLoadDataAtCurrentPage:outOf:withTotalNumberOf:)]) {
        int pages = [[responseObject valueForKey:@"last_page"] intValue];
        int currentPage = [[responseObject valueForKey:@"current_page"] intValue];
        int totalNumberOfArtworks = [[responseObject valueForKey:@"total"] intValue];
        [strongDelegate libraryDidLoadDataAtCurrentPage:currentPage outOf:pages withTotalNumberOf:totalNumberOfArtworks];
    }
}

- (void) loadGalleriesFromServerWithResponse:(id)responseObject
{
    Gallery *gallery;
    CLLocation* location;
    
    [persistencyManager clearGalleries];
    for (id itr in [responseObject valueForKey:@"data"]) {
        NSDictionary* tmpDict = itr;
        float latitude, longitude;
        
        latitude = [[tmpDict valueForKey:@"lat" ] floatValue];
        longitude = [[tmpDict valueForKey:@"lng" ] floatValue];
        location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        gallery = [[Gallery alloc] initWithId:[tmpDict valueForKey:@"id" ]
                                         name:[tmpDict valueForKey:@"name"]
                                      country:[tmpDict valueForKey:@"country"]
                                         city:[tmpDict valueForKey:@"city"]
                                      address:[tmpDict valueForKey:@"formatted_address"]
                                     postcode:[tmpDict valueForKey:@"postal_code"]
                                      placeId:[tmpDict valueForKey:@"place_id"]
                                        phone:[tmpDict valueForKey:@"phone"]
                                       rating:[tmpDict valueForKey:@"rating"]
                                  andLocation:location];
        
        [persistencyManager addGallery:gallery];
    }
    
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(libraryDidLoadGalleries)]) {
        [strongDelegate libraryDidLoadGalleries];
    }
}

- (void)geoLocationForAddressWithResponse: (id) responseObject
{
    float latitude, longitude;

    latitude = [[responseObject objectForKey:@"lat"] floatValue];
    longitude = [[responseObject objectForKey:@"lng"] floatValue];
    
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    if ([strongDelegate respondsToSelector:@selector(loadAddressWithLatitude:AndLongitude:)]) {
        [strongDelegate loadAddressWithLatitude:latitude AndLongitude: longitude];
    }
}

- (void) loadTagsFromServerWithResponse:(id) responseObject
{
    Tag *tag;
    
    [persistencyManager clearTags];
    
    for (id itr in [responseObject valueForKey:@"data"]) {
        tag = [[Tag alloc] initWithId:[itr valueForKey:@"id"]
                              AndName:[itr valueForKey:@"name"]];
        
        [persistencyManager addTag:tag];
    }
    
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(libraryDidLoadTags)]) {
        [strongDelegate libraryDidLoadTags];
    }
}

-(NSData *)dataFromBase64EncodedString:(NSString *)string
{
    if (string.length > 0) {
        
        //the iPhone has base 64 decoding built in but not obviously. The trick is to
        //create a data url that's base 64 encoded and ask an NSData to load it.
        NSString *data64URLString = [NSString stringWithFormat:@"data:;base64,%@", string];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:data64URLString]];
        return data;
    }
    return nil;
}

- (void) clearArtworks
{
    [persistencyManager clearArtworks];
}

- (void) showErrorViewWithTitle:(NSString *)title andError: (NSError*) error
{
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    if ([strongDelegate respondsToSelector:@selector(showErrorViewWithTitle:andError:)]) {
        [strongDelegate showErrorViewWithTitle:title andError:error];
    }
}

- (void) doLoginUserWithResponse:(id) responseObject
{
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    NSString *message = [responseObject valueForKey:@"message"];
    if ([message isEqualToString:@"success"]) {
        // NSLog(@"logged");
        NSString *avatar = [responseObject valueForKey:@"avatar" ];
        
        if (avatar == (id)[NSNull null]) {
            avatar = @"https://graph.facebook.com/534457/picture?type=large";
        }
        User *user = [User sharedInstance];
        [user setUserUid:[responseObject valueForKey:@"id" ]
                                     Name:[responseObject valueForKey:@"fullname" ]
                                 Username:[responseObject valueForKey:@"username" ]
                                    Email:[responseObject valueForKey:@"email" ]
                                   Avatar: [[NSURL alloc] initWithString:avatar]
                                   AndSex:[responseObject valueForKey:@"sex" ]];
        [user setIsLogged:YES];
        
        [user saveUser];

        if ([strongDelegate respondsToSelector:@selector(doUserLogin)]) {
            [strongDelegate doUserLogin];
        }
    } else {
        // NSLog(@"%@", [responseObject valueForKey:@"error"]);
        if ([strongDelegate respondsToSelector:@selector(userLoginFailedWithMessage:)]) {
            [strongDelegate userLoginFailedWithMessage:[responseObject valueForKey:@"error"]];
        }
    }
}

- (void) didLoadAppSettings:(NSDictionary *) settings
{
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    if ([strongDelegate respondsToSelector:@selector(didLoadAppSettings:)]) {
        [strongDelegate didLoadAppSettings:settings];
    }
}

- (void) didLoadBucketsWithResponse: (id) responseObject
{
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    Bucket *bucket;
    NSMutableArray *buckets = [[NSMutableArray alloc] init];
    
    for (id itr in [responseObject valueForKey:@"data"]) {
        bucket = [[Bucket alloc] initWithId:[itr valueForKey:@"id"]
                              AndName:[itr valueForKey:@"name"]];
        
        [buckets addObject:bucket];
    }
    
    NSMutableDictionary *data = [responseObject mutableCopy];
    [data setObject:buckets forKey:@"data"];
    
    [persistencyManager setBuckets: data];
    
    if ([strongDelegate respondsToSelector:@selector(libraryDidLoadBuckets)]) {
        [strongDelegate libraryDidLoadBuckets];
    }

}

- (void) didCreateBucketWithResponse: (id) responseObject
{
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    Bucket *bucket;
    
    if ([responseObject valueForKey:@"success"]) {
        bucket = [[Bucket alloc] initWithId:[responseObject valueForKey:@"id"]
                   AndName:[responseObject valueForKey:@"name"]];

        // if all buckets are already loaded push the new one, otherwise wait for the paginator
        if (([persistencyManager currentPageBuckets] >= [persistencyManager lastPageBuckets])
            && ![[persistencyManager getBuckets] containsObject:bucket]) {
            [persistencyManager addBucket:bucket];
        }
        if ([strongDelegate respondsToSelector:@selector(libraryDidLoadBuckets)]) {
            [strongDelegate libraryDidLoadBuckets];
        }
    }
}

- (void) didFinishAddingArtworkInBucketsWithResponse: (id) responseObject
{
    NSLog(@"Artwork added in buckets - %@", [responseObject valueForKey:@"success"]);
}

- (void) didLoadBucketedArtworksWithResponse: (id) responseObject {
    NSMutableDictionary *data = [responseObject mutableCopy];
    NSArray *tmpArtworks = [self parseArtworksResponse:responseObject];
    [data setObject:tmpArtworks forKey:@"data"];
    
    [persistencyManager setBucketedArtworks:data];
    
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    
    if ([strongDelegate respondsToSelector:@selector(libraryDidLoadBucketedArtworks)]) {
        [strongDelegate libraryDidLoadBucketedArtworks];
    }
}

- (void) didLoadArtworkWithResponse: (id) responseObject {
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    NSMutableArray* photos = [[NSMutableArray alloc] init];
    for (id photo in [responseObject valueForKey:@"pictures"]) {
        [photos addObject:[photo valueForKey:@"url"]];
    }
    
    
    NSArray *likes = [[NSArray alloc] initWithArray:[responseObject valueForKey:@"likes_count_relation"]];
    int likesCnt = 0;
    @try {
        likesCnt = [[[likes objectAtIndex:0] valueForKey:@"count"] intValue];
    }
    @catch (NSException *exception) {
        likesCnt = 0;
    }
    
    float latitude, longitude;
    
    latitude = [[[[responseObject valueForKey:@"gallery" ] valueForKey:@"location" ] valueForKey:@"lat" ] floatValue];
    longitude = [[[[responseObject valueForKey:@"gallery" ] valueForKey:@"location" ] valueForKey:@"lng" ] floatValue];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    NSDictionary *tmpGallery = [responseObject valueForKey:@"gallery"];
    Gallery *gallery = [[Gallery alloc] initWithId:[tmpGallery valueForKey:@"id"]
                                     name:[tmpGallery valueForKey:@"name"]
                                  country:[tmpGallery valueForKey:@"country"]
                                     city:[tmpGallery valueForKey:@"locality"]
                                  address:[tmpGallery valueForKey:@"formatted_address"]
                                 postcode:[tmpGallery valueForKey:@"postal_code"]
                                  placeId:[tmpGallery valueForKey:@"place_id"]
                                    phone:@""
                                   rating:@""
                              andLocation:location];
    
    Artwork *artwork = [[Artwork alloc] initWithTitle:[responseObject valueForKey:@"title"]
                                               author:[[responseObject valueForKey:@"author"] valueForKey:@"name" ]
                                                genre:[responseObject valueForKey:@"genre"]
                                              gallery:gallery
                                          description:[responseObject valueForKey:@"description"]
                                            technique:[responseObject valueForKey:@"technique"]
                                               photos:photos
                                                 tags:[responseObject valueForKey:@"tags"]
                                             comments:[responseObject valueForKey:@"comments"]
                                                likes:likesCnt
                                                price:[responseObject valueForKey:@"price"]
                                           dimensions:[responseObject valueForKey:@"dimensions"]
                                          geolocation:location
                                                  uid:[responseObject valueForKey:@"id"]
                                              buckets:[responseObject valueForKey:@"buckets"]
                                              andYear:[responseObject valueForKey:@"year"]];
    
    if ([strongDelegate respondsToSelector:@selector(libraryDidLoadArtwork:)]) {
        [strongDelegate libraryDidLoadArtwork:artwork];
    }
}

- (void) didLoadGalleryDetailsWithResponse: (id) responseObject {
    id <LibraryAPIDelegate> strongDelegate = self.delegate;
    NSDictionary *rawData = [responseObject valueForKey:@"result"];
    if (rawData) {
        NSString *country = @"", *city = @"";
        for (id addr in [rawData valueForKey:@"address_components"]) {
            NSArray* types = [addr valueForKey:@"types"];

            if ([types containsObject:@"country"]) {
                country = [addr valueForKey:@"long_name"];
            } else if ([types containsObject:@"postal_town"] || [types containsObject:@"locality"]) {
                city = [addr valueForKey:@"long_name"];
            }
        }
        NSDictionary *openingHours = [rawData valueForKey:@"opening_hours"];
        NSMutableArray *visitingHours = [[NSMutableArray alloc] init];
        BOOL isOpen = NO;
        if (openingHours != nil) {
            IKTVisitingPeriod *visitingPeriod;
            for (id period in [openingHours valueForKey:@"periods"]) {
                NSDictionary *open = [period valueForKey:@"open"];
                NSDictionary *close = [period valueForKey:@"close"];
                NSString *closeTime = @"";
                if (close) {
                    closeTime = [close valueForKey:@"time"];
                }
                visitingPeriod = [[IKTVisitingPeriod alloc] initWithDay:[[open valueForKey:@"day"] integerValue]
                                                                   open:[open valueForKey:@"time"]
                                                                  close:closeTime];
                [visitingHours addObject:visitingPeriod];
            }
            isOpen = [[openingHours valueForKey:@"open_now"] boolValue];
        }

        NSString *name = [rawData valueForKey:@"name"],
        *address = [rawData valueForKey:@"formatted_address"],
        *phone = [rawData valueForKey:@"international_phone_number"],
        *email = @"",
        *placeId = [rawData valueForKey:@"place_id"];
        
        if (nil == address) {
            address = @"";
        }
        if (nil == phone) {
            phone = @"";
        }
        
        IKTGalleryDetails *galleryDetails = [[IKTGalleryDetails alloc] initWithName:name
                                                                            country:country
                                                                               city:city
                                                                            address:address
                                                                              phone:phone
                                                                              email:email
                                                                            placeId:placeId
                                                                      visitingHours:visitingHours
                                                                            openNow:isOpen];

        if ([strongDelegate respondsToSelector:@selector(libraryDidLoadGalleryDetails:)]) {
            [strongDelegate libraryDidLoadGalleryDetails:galleryDetails];
        }
    }
    
}
@end
