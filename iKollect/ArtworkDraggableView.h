//
//  ArtWorkDraggableView.h
//  iKollect
//
//  Created by mihata on 5/14/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ArtworkDraggableViewDelegate;

@interface ArtworkDraggableView : UIView
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *author;
@property (strong, nonatomic) UILabel *dimensions;

@property (nonatomic, weak) id <ArtworkDraggableViewDelegate> delegate;

//-(void) loadImageAndStyle;
@end

@protocol ArtworkDraggableViewDelegate <NSObject>

- (void)artworkDraggableDidEndSwipingWithStrength:(CGFloat) strength;

@end

