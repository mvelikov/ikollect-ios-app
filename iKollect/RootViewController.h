//
//  RootViewController.h
//  iKollect
//
//  Created by mihata on 9/5/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface RootViewController : GAITrackedViewController <LibraryAPIDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *initializeButton;
@property (weak, nonatomic) IBOutlet UIView *loadingIndicatorWrapper;

- (IBAction)initializeButtonTapped:(id)sender;
@end
