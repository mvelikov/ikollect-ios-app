//
//  AboutViewController.h
//  iKollect
//
//  Created by mihata on 9/10/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKTParentViewController.h"

@interface AboutViewController : IKTParentViewController
@property (weak, nonatomic) IBOutlet UIButton *contactButton;
@property (weak, nonatomic) IBOutlet UIButton *privacyPolicyButton;
@property (weak, nonatomic) IBOutlet UIButton *termsAndConditionaButton;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *buildLabel;

- (IBAction)contactButtonTapped:(id)sender;
- (IBAction)privacyPolicyButtonTapped:(id)sender;
- (IBAction)termsAndConditionsButtonTapped:(id)sender;

@end
