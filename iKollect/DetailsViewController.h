//
//  DetailsViewController.h
//  iKollect
//
//  Created by mihata on 5/17/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "MasterArtworkViewController.h"
#import "Artwork.h"
#import "BucketPickerTableViewController.h"
#import "GAITrackedViewController.h"
#import <MapKit/MapKit.h>

@interface DetailsViewController : GAITrackedViewController <MKMapViewDelegate, LibraryAPIDelegate, BucketPickerDelegate>
@property (nonatomic, weak) Artwork *detailsObject;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *authorLabel;
@property (nonatomic, weak) IBOutlet UILabel *dimensionsLabel;
@property (nonatomic, weak) IBOutlet UILabel *galleryLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet MKMapView *artworkMapView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addToBucketButton;
@property (nonatomic, weak) IBOutlet UIImageView *mainImage;
@property (weak, nonatomic) IBOutlet UILabel *likesLabel;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *galleryDetailsButton;

- (IBAction)imageButtonTapped:(id)sender;
- (IBAction)shareButtonTapped:(id)sender;
-(void) setDetailsObject:(Artwork *)object;
@end
