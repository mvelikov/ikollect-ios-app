//
//  IKTBasePaginatedTableViewController.h
//  IKOLLECT
//
//  Created by mihata on 10/9/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAI.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "GAIFields.h"

@interface IKTBasePaginatedTableViewController : UITableViewController  {
    
}

@property (nonatomic) NSUInteger currentPage, pages, totalItems;
@property (strong, nonatomic) NSMutableArray *items;

- (UITableViewCell *)tableView:(UITableView *)tableView loadingCellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void) preloadItems;
@end
