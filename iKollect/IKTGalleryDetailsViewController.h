//
//  IKTGalleryDetailsViewController.h
//  IKOLLECT
//
//  Created by Mihail Velikov on 10/15/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOLLECT-Swift.h"
#import "GAITrackedViewController.h"


@interface IKTGalleryDetailsViewController : GAITrackedViewController <LibraryAPIDelegate>

@property (strong, nonatomic) IKTGalleryDetails *details;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *detailsActivityIndicator;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIButton *addressButton;
@property (weak, nonatomic) IBOutlet UIButton *phoneButton;
@property (weak, nonatomic) IBOutlet UITableView *visitingHoursTableView;

- (IBAction)phoneButtonTapped:(id)sender;
- (IBAction)addressButtonTapped:(id)sender;
@end
