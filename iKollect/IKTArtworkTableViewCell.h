//
//  IKTArtworkTableViewCell.h
//  IKOLLECT
//
//  Created by mihata on 10/7/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKTArtworkTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *picture;
@property (weak, nonatomic) IBOutlet UILabel *title;
@end
