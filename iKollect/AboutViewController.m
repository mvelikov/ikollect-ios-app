//
//  AboutViewController.m
//  iKollect
//
//  Created by mihata on 9/10/14.
//  Copyright (c) 2014 mihata. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutDetailsViewController.h"
#import "SWRevealViewController.h"
#import <MessageUI/MessageUI.h>

@interface AboutViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *sidebarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu.png"] style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = sidebarButton;
    
    [_versionLabel setText:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    [_buildLabel setText:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.screenName = @"About Screen";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)contactButtonTapped:(id)sender {
    MFMailComposeViewController* mailer = [[Helper sharedInstance] getMailComposerViewControllerWithRecipient:@"contact@ikollect.net"
                                                                                   Subject:@"Contact iKollect"
                                                                                   andBody:@""];
    mailer.mailComposeDelegate = self;
    [self presentViewController:mailer animated:YES completion:nil];
}

- (IBAction)privacyPolicyButtonTapped:(id)sender {
    
}

- (IBAction)termsAndConditionsButtonTapped:(id)sender {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    AboutDetailsViewController *controller = [segue destinationViewController];
    
    if (sender == _termsAndConditionaButton) {
        controller.title = @"Terms of Use";
        controller.aboutUrl = [NSString stringWithFormat:@"%@/terms-of-use", PAGES_URL];
    } else if (sender == _privacyPolicyButton) {
        controller.title = @"Privacy Policy";
        controller.aboutUrl = [NSString stringWithFormat:@"%@/privacy-policy", PAGES_URL];
    }
}

#pragma mark MFMailComposeViewControllerDelegate delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
